use strict;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Irssi;
use vars qw($VERSION %IRSSI);

$VERSION = 0.1;
%IRSSI = (
    authors     => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house',
    name        => 'MUCK',
    description => 'MUCK-like commandline stuff for irssi',
    license     => 'MIT',
    changed     => '9/15/2006',
);

my $lastpaged = '';

Irssi::signal_add "send text" => sub ($data, $server, $witem) {
    if ($data =~ /^:(d|x|d|o|p|v|\(|\)|\\|\[|\]|[0-9]|\||<|>)(\!|\*|\s|$)/i) {
        $witem->command("say $data");
        Irssi::signal_stop;
    }
    elsif ($data =~ /^:/) {
        $data =~ s/^://;
        $witem->command("me $data");
        Irssi::signal_stop();
    }
    else {
        my ($trigger, $text) = split / /, $data, 2;

        if ($trigger =~ /^ooc$/) {
            $witem->command("say (($text))");
            Irssi::signal_stop();
        } elsif ($trigger =~ /^tb$/) {
            $witem->command("say . o O ($text)");
            Irssi::signal_stop();
        } elsif ($trigger eq "pose") {
            $witem->command("me $text");
            Irssi::signal_stop();
        }
        elsif ($trigger =~ /^(p(age)|w(hisper))$/) {
            my ($target,$text2) = split /=/,$text,2;
            unless ($target eq '#r') {
                $lastpaged = $target;

                $witem->command("msg $target $text2");
            }
            else {
                $witem->command("msg $lastpaged $text2");
            }
            Irssi::signal_stop();
        }
    }
};
Irssi::signal_add "message private" => sub ($server, $data, $nick, $address) {
    return unless $server;
    $lastpaged = $nick;
};
