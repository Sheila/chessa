use strict;
use Irssi;
use vars qw($VERSION %IRSSI);

$VERSION = 0.1;
%IRSSI = (
    name        => 'HTML Log',
    description => 'Rewrite certain types of messages into HTML',
    author      => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house'
);
my $LOGGING = 1;
my $GM_NICK = 'ajoxer';
my %players = (
    Adam => {
	tag => 'AE',
	colour => '#00BB00',
	chum => 'anachronElegy',
    },
    BuckToren => {
	tag => 'DD',
	colour => '#772EF1',
	chum => 'dermalDecorator',
    },
    Tora => {
	tag => 'ED',
	colour => '#9896E1',
	chum => 'extendedDissonance',
    },
    Will => {
	tag => 'GO',
	colour => '#808080',
	chum => 'gaseousOphidian',
    },
    Gillian => {
	tag => 'QC',
	colour => '#F49101',
	chum => 'quintessentialCompetitor',
    },
    YueBlackmoor => {
	tag => 'SF',
	colour => '#00BBBB',
	chum => 'sagaciousFairy',
    },
    Mireille => {
	tag => 'SS',
	colour => '#FF4545',
	chum => 'sanguineSentience',
    },
    JoyGoodwin => {
        tag => 'EA',
        colour => '#b00',
	chum => 'enthusiasticApocalypse',
    },
);

Irssi::signal_add 'event privmsg' => sub {
    my ($server, $data, $nick, $address) = @_;
    my ($target, $text) = split / :/, $data, 2;

    return unless ($target =~ /^#a(g_lounge|eons)$/i);

    if (lc($nick) eq $GM_NICK) {
        if ($text =~ /^\*\*LOG START\*\*$/) {
            $LOGGING = 1;
	    return;
        } elsif ($text =~ /^\*\*LOG (?:END|STOP)\*\*$/) {
            $LOGGING = 0;
	    return;
        } else {
            return;
        }
    }

    return unless $LOGGING == 1;
    return unless defined($players{$nick});

    $text =~ s/\cC(\d+)?(?:,\d+)?//g;

    my $tag = $players{$nick}{tag};
    my $col = $players{$nick}{colour};

    if ($text =~ /^$tag:/) {
    	$text = qq(<span style="color: $col">$text</span>);
    } else {
	my $chum = $players{$nick}{chum};
	return unless $text =~ /$chum/;

	for my $char (keys %players) {
	    $tag = $players{$char}{tag};
	    $col = $players{$char}{colour};
	    $text =~ s{($chum \[$tag\])}{<span style="color: $col">$1</span>}g;
	}
    }

    Irssi::signal_continue($server, "$target :$text", $nick, $address);
};

