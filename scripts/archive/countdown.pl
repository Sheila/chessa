use strict;
use warnings;
use feature qw(:5.14);

no if $] >= 5.018, warnings => "experimental::smartmatch";

use GDBM_File;

use Irssi;

use Chessa::Countdown;

our $VERSION = 0.1;
our %IRSSI = (
    name        => 'countdown.pl',
    description => 'irssi interface to Chessa::Countdown',
    authors     => 'Síle Ekaterin Liszka',
    license     => 'MIT'
);

my %db;
tie %db, 'GDBM_File', Irssi::get_irssi_dir . '/countdown.db', &GDBM_WRCREAT, 0640;
unless (exists($db{created})) {
    $db{created} = 1;

    $db{christmas}         = '%Y-12-25T00:00:00';
    $db{halloween}         = '%Y-10-31T00:00:00';
    $db{"valentine's day"} = '%Y-02-14T00:00:00';
}

Irssi::signal_add 'event privmsg' => sub {
    my ($server, $data, $nick, $address) = @_;
    my ($target, $text) = split / :/, $data, 2;

    if ($target =~ /^#/) {
        $target .= " $nick:";
    } else {
        $target = $nick;
    }

    my $trigger;
    ($trigger, $text) = split ' ', $text, 2;

    given ($trigger) {
        when ('.countdown') {
            if ($text =~ /^20\d{2}-\d{2}-\d{2}T\d{2}:\d{2}(?::\d{2})$/) {
                $text =~ s/$/:00/ if ($text =~ /T\d{2}:\d{2}$/);
                my $c = Chessa::Countdown->new(date => $text);
                my $ret = $c->delta;
                $server->command("msg $target $ret");
            } elsif (exists($db{lc($text)})) {
                my $c = Chessa::Countdown->new(date => $db{lc($text)});
                my $ret = $c->delta;

                if ($ret =~ /^-/) {
                    $ret =~ s/^-//;
                    $server->command("msg $target Countdown for '$text' has expired; purging. ($ret has since passed.)");
                    delete $db{lc($text)};
                } else {
                    $server->command("msg $target $ret until $text");
                }
            }
        } when ('.addcount') {
            my ($date, $label) = split ' ', $text, 2;
            if ($date =~ /^(?:20\d{2}|%Y)-\d{2}-\d{2}T\d{2}:\d{2}(?::\d{2})$/) {
                $date =~ s/$/:00/ if ($date =~ /T\d{2}:\d{2}$/);
            } else {
                return;
            }
            $db{lc($label)} = $date;
            my $c = Chessa::Countdown->new(date => $db{lc($label)});
            my $ret = $c->delta;
            $server->command("msg $target Added $date as '$label'; ETA is currently $ret");
        } when ('.delcount') {
            if (exists($db{lc($text)})) {
                delete $db{lc($text)};
                $server->command("msg $target Deleted countdown for '$text'");
            }
        } when ('.listcount') {
            $server->command("msg $target Listing " . scalar(keys %db) . " countdowns...");
            for my $key (keys %db) {
                $server->command("msg $target $key: $db{$key}");
            }
        }
    }
};

