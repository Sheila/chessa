use strict;
use warnings;
use feature qw(:5.10);

no if $] >= 5.018, warnings => "experimental::smartmatch";

use List::Util qw(min sum);
use POSIX qw(ceil);
use Irssi;

our $VERSION = 0.4;
our %IRSSI = (
    name        => 'FFRPG Levels',
    description => 'Calculates experience and gil for given levels.',
    authors     => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house',
    license     => 'MIT',
);

sub range {
    my ($start, $stop, $step) = @_;
    my @iter;

    unless (defined($stop)) {
	($stop, $start, $step) = ($start, 0, 1);
    }
    $step = 1 unless defined $step;

    for (my $x = $start; $x < $stop; $x += $step) {
	push @iter, $x;
    }

    return @iter;
}

my @gil3_mul = ([7, 4, 750], [11, 10, 1000], [21, 10, 1500], [31, 10, 1000]);

sub gil3 {
    my ($level, $mod) = @_;
    my ($base, $other);
    my @iter = range(0, 600, 100);
    $base = 500 + sum(@iter[0 .. ($level - 1)]);

    foreach my $i (@gil3_mul) {
	my $x = $level - $i->[0] + 1;
	if ($x > 0) {
	    $base += min($x, $i->[1]) * $i->[2];
	}
    }
    if ($level >= 65) {
	$other = '2 Artifacts, 1 Legendary'
    } elsif ($level >= 50) {
	$other = '1 Artifact'
    }

    $mod =~ s/%$//g;
    $base *= (1+($mod/100))      if (defined($mod) && ($mod > 0));
    $base *= (1-(abs($mod)/100)) if (defined($mod) && ($mod < 0));

    return join(', ', ($base, $other)) if defined($other);
    return $base;
}

my @enggil3_mul = ([7, 4, 150], [11, 10, 200], [21, 10, 300], [31, 20, 200]);

sub enggil3 {
    my ($level, $mod) = @_;
    my ($base, $other);
    my @iter = (0, 120, 40, 60, 80, 100);
    $base = 100 + sum(@iter[0 .. ($level - 1)]);

    foreach my $i (@enggil3_mul) {
	my $x = $level - $i->[0] + 1;
	if ($x > 0) {
	    $base += min($x, $i->[1]) * $i->[2];
	}
    }
    if ($level >= 65) {
	$other = '1 Artifact'
    }

    $mod =~ s/%$//g;
    $base *= (1+($mod/100))      if (defined($mod) && ($mod > 0));
    $base *= (1-(abs($mod)/100)) if (defined($mod) && ($mod < 0));

    return join(', ', ($base, $other)) if defined($other);
    return $base;
}

sub xp {
    my $level = $_[0];
    my $exp = 500 * ($level - 1) * ($level / 2);
    return $exp;
}
sub avail3 {
    my ($level) = @_;
    return ($level > 10) ? int (92-10-($level-10)*1.25) : (92-$level);
}
sub availlev {
    my ($avail) = @_;
    return ($avail < 82) ? 10 + int((82-$avail)/1.25) : 92 - $avail;
}

sub level3 {
    my ($level, $mod) = @_;
    
    $mod = undef unless $mod =~ /^-?\d{1,2}%?$/;

    my ($xp, $next, $avail, $gil, $enggil, $skillmax);

    $xp      = xp($level);
    $next     = ($level >= 99) ? 0 : $level * 500;
    $avail    = avail3($level);
    $gil      = gil3($level);
    $enggil   = enggil3($level);
    $skillmax = 50 + (($level - 1) * 2);

    if (defined($mod)) {
	$mod =~ s/^(\d{1,2})/$1/;

	$gil = int($gil * (1 + ($mod / 100)));
	$enggil = int($enggil * (1 + ($mod / 100)));
    }


    $skillmax = 100 unless ($skillmax < 100);

    return "Lv$level: \cBXP\cB: $xp \cBNext\cB: $next \cBAvail\cB: $avail \cBGil\cB: $gil \cBENG Bonus\cB: $enggil \cBSkill Cap\cB: $skillmax";
}

sub aslot {
    my ($level) = @_;

    given ($level) {
        when ($_ < 10) {
            return int( $level / 3 + 1 );
        } when ($_ < 30) {
            return int( 4 + (($level - 10) / 4) );
        } when ($_ < 65) {
            return int( 9 + (($level - 30) / 7) );
        }
    }
    return 14;
}
sub tslot {
    my ($level) = @_;

    given($level) {
        when ($_ < 5) {
            return 3;
        } when ($_ < 45) {
            return int( 3 + (($level - 4) / 5) );
        }
    }
    return 12;
}

sub levels {
    my ($level) = @_;
    
    my $exp = xp($level);
    my $next = 500 * $level;

    my $tier = ($level >= 100) ? 10 : int(($level - 1)  / 10) + 1;
    my $aslot = 10 + int(($level - 1) / 2);
    my $tslot = tslot($level);
    my $pslot = 10 + int($level / 5);
    my $jxp = ($level == 1) ? 0 : int(($level * (3/4) - 1 ) * 50);

    return "Lv$level: \cBXP\cB: $exp \cBNext\cB: $next \cBTier\cB: $tier \cBActive\cB: $aslot \cBPassive\cB: $pslot \cBTalents\cB: $tslot \cBJP\cB: $jxp";
}


my @rungs = (
    [ 1, 10, 10],
    [11, 10, 20],
    [21, 10, 30],
    [31,  2, 50]
);
my @grist = ('0',
            '10',   '20',   '50',
           '100',  '200',  '500',
            '1k',   '2k',   '5k',
           '10k',  '20k',  '50k',
          '100k', '200k', '500k',
            '1m',   '2m',   '5k',
           '10m',  '20m',  '50k',
          '100m', '200m', '500m',
            '1b',   '2b',   '5b',
           '10b',  '20b',  '50b',
	  '100b', '200b',
);

sub rung {
    my ($rung) = @_;
    my ($xp, $skill, $grist, $dots);
    my $tier = 'N/A';

    $rung = 0 if $rung < 0;
    $rung = 32 if $rung > 32;

    $skill = 3 + int(($rung - 1) / 3);
    $grist = $grist[$rung];
    $dots = ceil($rung / 5);
    $xp = 0;
    $dots .= ($dots > 1) ? ' dots' : ' dot';

    if ($rung > 0) {
        foreach my $i (@rungs) {
            my $x = $rung - $i->[0] + 1;
	    
            if ($x > 0) {
                $xp += min($x, $i->[1]) * $i->[2];
            }
        }
    }
    given ($rung) {
	when (($_ > 0) && ($_ < 8)) {
	    $tier = 'Pawn'
        } when (($_ > 7) && ($_ < 28)) {
	    $tier = 'Noble'
	} when ($_ > 27) {
	    $tier = 'Royal'
	}
    }
    return "Rung $rung: \cBXP\cB: $xp \cBHP\cB: +$xp \cBStrife/DV\cB: +$rung \cBSkill Cap\cB: $skill \cBGrist\cB: $grist ($dots) \cBTier\cB: $tier";
}

sub cmd_process {
    my ($server, $trigger, $text, $nick, $target) = @_;
    $target .= " $nick:" if ($target =~ /^[#&]/);

    given ($trigger) {
        when ('level') {
            my ($level, $mod) = split / /, $text, 2;

            $server->command("msg $target ".level3($level, $mod));
        } when ('seedlv') {
            $server->command("msg $target ".levels($text));
        } when ('avail') {
            my $level = availlev($text);
            $server->command("msg $target Availability $text at Lv$level");
        } when ('rung') {
	    $server->command("msg $target ".rung($text));
	}
    }
}

Irssi::signal_add 'event privmsg' => sub {
    my ($server, $data, $nick, $address) = @_;
    my ($target, $text) = split / :/, $data, 2;
    my ($me, $hey) = ($server->{nick}, undef);

    return if ($target =~ /^#(triumph|dwooc)/i);

    unless ($target =~ /^[#&]/) {
        $target = $nick;
    }
    if ($text =~ /^(?:$me)[,:]? /) {
        $text =~ s/(?:$me)[,:]? //;
        if ($text !~ /^\./) {
            my ($trigger, $str) = split / /, $text, 2;
            cmd_process($server, $trigger, $str, $nick, $target);
        }
    } elsif ($text =~ /^\./) {
        $text =~ s/^\.//;
        my ($trigger, $str) = split / /, $text, 2;
        cmd_process($server, $trigger, $str, $nick, $target);
    }
};


