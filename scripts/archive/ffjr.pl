#!/usr/bin/perl

use strict;
use DBI;
use Irssi;
use vars qw($VERSION %IRSSI $HOME);

$VERSION = 0.2;
%IRSSI = (
	name        => 'FFRPG Races & Jobs',
	description => 'Retrieves information about FFRPG races & jobs from a database',
	authors     => 'Síle Ekaterin Liszka',
	contact     => 'sheila@vulpine.house',
	license     => 'MIT',
);
$HOME = Irssi::get_irssi_dir;

my $db = DBI->connect("dbi:SQLite:dbname=$HOME/ffrpg.db", '', '');
my $sth = $db->prepare('SELECT tla FROM job;');
$sth->execute();
my @tla;
push (@tla, $_) while ($_ = $sth->fetchrow_array);
$sth = $db->prepare('SELECT tla FROM ojob;');
$sth->execute();
push (@tla, $_) while ($_ = $sth->fetchrow_array);
$sth = undef;

sub tc {
	my @str = split / /, lc($_[0]);
	my $str;
	foreach my $i (@str) {
		$i = uc(substr($i,0,1)).substr($i,1);
		$str .= "$i ";
	}
	$str =~ s/\s+$//;
	return $str;
}
sub random_race {
	my ($ord) = @_;
	my $sth = $db->prepare('SELECT name FROM race;');
	$sth->execute();
	my @races;
	push (@races, $_) while ($_ = $sth->fetchrow_array);
	if (defined($ord)) {
		$sth = $db->prepare('SELECT name FROM orace;');
		$sth->execute();
		push (@races, $_) while ($_ = $sth->fetchrow_array);
	}
	
	return $races[int(rand(scalar(@races)))];
}
sub random_job {
	my ($ord) = @_;
	my @jobs;
	my $sth = $db->prepare('SELECT name FROM job;');
	$sth->execute();
	push (@jobs, $_) while ($_ = $sth->fetchrow_array);
	if (defined($ord)) {
		$sth = $db->prepare('SELECT name FROM ojob;');
		$sth->execute();
		push (@jobs, $_) while ($_ = $sth->fetchrow_array);
	}
	
	return $jobs[int(rand(scalar(@jobs)))];
}

sub char {
	my ($race, $job, $ord) = @_;
	my ($r, $j);
	my ($str, $vit, $agi, $spd, $mag, $spr);

	my $sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr FROM race WHERE name=?;');
	$sth->execute($race);
	unless ($r = $sth->fetchrow_arrayref) {
		if (defined($ord)) {
			$sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr FROM orace WHERE name=?;');
			$sth->execute($race);
			unless ($r = $sth->fetchrow_arrayref) {
				$race = random_race($ord);
				return char($race, $job, $ord);
			}
		} else {
			$race = random_race($ord);
			return char($race, $job, $ord);
		}
	}
	$sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr, url FROM job WHERE name=?;');
	$sth->execute($job);
	unless ($j = $sth->fetchrow_arrayref) {
		if (defined($ord)) {
			$sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr, url FROM ojob WHERE name=?;');
			$sth->execute($job);
			unless ($j = $sth->fetchrow_arrayref) {
				$job = random_job($ord);
				return char($race, $job, $ord);
			}
		} else {
			$job = random_job($ord);
			return char($race, $job, $ord);
		}
	}
	($str, $vit, $agi, $spd, $mag, $spr) = (
		$r->[1] + $j->[1],
		$r->[2] + $j->[2],
		$r->[3] + $j->[3],
		$r->[4] + $j->[4],
		$r->[5] + $j->[5],
		$r->[6] + $j->[6],
	);
	my $url = $j->[7];
	$url =~ s!^a!http://www.returnergames.com/ord/index.php!;
	$url =~ s!^b!http://storagebin.wikispaces.com!;
	$url =~ s!^c!http://terra-chan.wikispaces.com!;

	return "$r->[0] $j->[0] - $str/$vit/$agi/$spd/$mag/$spr (URL: $url )";
}
sub race {
	my ($race, $ord) = @_;
	my ($r);

	if ($race eq 'list') {
	    my $sth;
	    if (defined($ord)) {
		$sth = $db->prepare('SELECT name FROM orace;');
	    } else {
		$sth = $db->prepare('SELECT name FROM race;');
	    }
	    $sth->execute();
	    my @races;
	    push(@races, $_) while ($_ = $sth->fetchrow_array);
	    return join(', ', @races);
	}

	$race = random_race($ord) if (!defined($race) || ($race eq '*'));

	my $sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr FROM race WHERE name=?;');
	$sth->execute($race);
	unless ($r = $sth->fetchrow_arrayref) {
		if (defined($ord)) {
			$sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr FROM orace WHERE name=?;');
			$sth->execute($race);
			unless ($r = $sth->fetchrow_arrayref) {
				$race = random_race($ord);
				return race($race, $ord);
			}
		} else {
			$race = random_race($ord);
			return race($race, $ord);
		}
	}
	return "$r->[0] - $r->[1]/$r->[2]/$r->[3]/$r->[4]/$r->[5]/$r->[6]\n";
}
sub job {
	my ($job, $ord) = @_;
	my $j;

	$job = random_job($ord) if (!defined($job) || ($job eq '*'));

	if ($job eq 'list') {
	    my $sth;
	    if (defined($ord)) {
    		$sth = $db->prepare('SELECT name FROM ojob;');
	    } else {
		$sth = $db->prepare('SELECT name FROM job;');
	    }
	    $sth->execute();
	    my @jobs;
	    push(@jobs, $_) while ($_ = $sth->fetchrow_array);
	    return join(', ', @jobs);
	}

	my $sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr, url FROM job WHERE name=?;');
	$sth->execute($job);
	unless ($j = $sth->fetchrow_arrayref) {
		if (defined($ord)) {
			$sth = $db->prepare('SELECT name, str, vit, agi, spd, mag, spr, url FROM ojob WHERE name=?;');
			$sth->execute($job);
			unless ($j = $sth->fetchrow_arrayref) {
				$job = random_job($ord);
				return job($job, $ord);
			}
		} else {
			$job = random_job($ord);
			return job($job, $ord);
		}
	}
	my $url = $j->[7];
	$url =~ s!^a!http://www.returnergames.com/ord/index.php!;
	$url =~ s!^b!http://storagebin.wikispaces.com!;
	$url =~ s!^c!http://terra-chan.wikispaces.com!;

	return "$j->[0] - $j->[1]/$j->[2]/$j->[3]/$j->[4]/$j->[5]/$j->[6] (URL: $url )";
}
sub job_tla {
	my ($tla) = @_;
	my $j;

	if (length($tla) == 3) {
		my $sth = $db->prepare('SELECT name FROM job WHERE tla=?;');
		$sth->execute($tla);
		unless ($j = $sth->fetchrow_arrayref) {
			$sth = $db->prepare('SELECT name FROM ojob WHERE tla=?;');
			$sth->execute($tla);
			unless ($j = $sth->fetchrow_arrayref) {
				return "unknown";
			}
		}
	} else {
		$tla = tc($tla);
		my $sth = $db->prepare('SELECT tla FROM job WHERE name=?;');
		$sth->execute($tla);
		unless ($j = $sth->fetchrow_arrayref) {
			$sth = $db->prepare('SELECT tla FROM ojob WHERE name=?;');
			$sth->execute($tla);
			unless ($j = $sth->fetchrow_arrayref) {
				return "unknown";
			}
		}
	}
	return $j->[0];
}
sub parse_race {
	my ($race, $ord) = @_;
	return $race if $race =~ /^list$/i;
	$race = random_race($ord) if (!defined($race) || ($race eq '*'));

	$race = 'nu mou'   if (lc($race) eq "n'mou");
	$race = 'tarutaru' if (lc($race) eq 'taru');
	$race = 'elvaan'   if (lc($race) eq 'elf');

	return tc($race);
}
sub parse_job {
	my ($job, $ord) = @_;
	my (@job, $j);

	return $job if $job =~ /^list$/i;

	$job = random_job($ord) if (!defined($job) || ($job eq '*'));

	$job = 'WHT' if ($job eq 'WHM');
	$job = 'BLK' if ($job eq 'BLM');

	if (length($job) == 3) {
		my @alc = qw(ALA ALM);
		$job = $alc[int(rand(scalar(@alc)))] if (lc($job) eq 'alc');
		@job = grep { lc($_) eq lc($job) } @tla;
		$job = job_tla($job[0]);
	} else {
		my @alc = ('Alchemist (Adept)', 'Alchemist (Mage)');
		$job = $alc[int(rand(scalar(@alc)))] if (lc($job) eq 'alchemist');
		$job = tc($job);
	}

	return $job;
}
sub parse_crj {
	my ($str, $ord) = @_;
	my (@str) = split / /, $str;
	my ($race, $job);

	if (lc($str[0]) eq 'nu') {
		$race = shift @str;
		i$race .= ' '.shift @str;
	} else {
		$race = shift @str;
	}
	$race = parse_race($race, $ord);

	$job = join ' ', @str;
	$job = parse_job($job, $ord);
	return ($race, $job);
}

Irssi::signal_add 'event privmsg' => sub {
	my ($server, $data, $nick, $address) = @_;
	my ($target, $text) = split / :/,$data,2;
	my $target2 = $target;
	$target = ($target =~ /^#/) ? "$target $nick:" : $nick;
	my ($trigger, $str) = split / /, $text, 2;

	return if ($target =~ /^#(triumph|dwooc)/i);

	if ($trigger =~ /^\.(ord)?char$/) {
		my $ord = 1 if ($trigger eq '.ordchar');
		my ($race, $job) = parse_crj($str, $ord);
		my $string = char($race, $job, $ord);
		$server->command("msg $target $string");
	} elsif ($trigger =~ /^\.(ord)?race$/) {
		my $ord = 1 if ($trigger eq '.ordrace');
		my ($race) = parse_race($str, $ord);
		my $string = race($race, $ord);
		$server->command("msg $target $string");
	} elsif ($trigger =~ /^\.(ord)?job$/) {
		my $ord = 1 if ($trigger eq '.ordjob');
		my ($job) = parse_job($str, $ord);
		my $string = job($job, $ord);
		$server->command("msg $target $string");
	} elsif ($trigger eq '.tla') {
		my $tla = job_tla($str);
		$server->command("msg $target $str - $tla");
	}
};
