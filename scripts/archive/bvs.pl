use strict;
use Irssi;
use vars qw($VERSION %IRSSI);
use Date::Format qw(time2str);

$VERSION = 0.1;

%IRSSI = {
    name        => 'Billy vs Snakeman',
    description => 'Informational triggers for the BvS browser-based game.',
    author      => 'Síle Ekaterin Liszka',
    license     => 'MIT',
};

my $current_darkhour_x = 3;

sub darkhour {
    my $x = $current_darkhour_x;
    my @time = 

    my $ret = 'Dark Hours for '.time2str('%A, %B %d, %Y: ', time);

    my $vals = sprintf('%02d:00, %02d:00, %02d:00, %02d:00, %02d:00',
        $x, $x + 4, $x + 14, $x + 17, $x + 21 - 24);

    $ret .= $vals;

    return $ret;
}

Irssi::command_bind 'dayroll' => sub {
    my ($data, $server, $witem) = @_;

    $current_darkhour_x++;
    if ($current_darkhour_x > 6) {
	$current_darkhour_x = 3;
    }
};

Irssi::signal_add 'event privmsg' => sub {
    my ($server, $data, $nick, $address) = @_;
    my ($target, $text) = split / :/, $data, 2;

    my ($trigger, $input) = split / /, $text, 2;

    if ($target !~ /^[#&]/) {
        $target = $nick;
    } else {
        $target = "$target $nick:"
    }

    if ($trigger eq '.darkhour') {
        if ($input eq '') {
            my $dk = darkhour;

            $server->command("msg $target $dk");
        } elsif ($input =~ s/^set (\d)$/$1/) {
            $current_darkhour_x = $input;

            my $dk = darkhour;

            $server->command("msg $target $dk");
        }
    }
};


