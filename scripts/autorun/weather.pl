
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);
use autodie qw(:all);

no if $] >= 5.018, warnings => qw(experimental::smartmatch);

use DBI;

use Irssi;

use Chessa::Weather;

our $VERSION = 0.3;
our %IRSSI = (
    name        => 'weather.pl',
    description => 'Weather integration for irssi',
    authors     => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house',
    license     => 'ISC',
);

Irssi::settings_add_str 'weather', 'weather_api_key', '';
Irssi::settings_add_str 'weather', 'weather_geo_key', '';

my $db = DBI->connect('dbi:SQLite:dbname=/home/chessa/weather_sql.db', '', '');

unless (-e '/home/chessa/.irssi/weather2_guard') {
    my $sth = $db->prepare('CREATE TABLE weather2 (nick VARCHAR(30), country VARCHAR(2), location VARCHAR(128));');
    if (defined($sth)) {
        $sth->execute();
        open my $fh, '>', '/home/chessa/.irssi/weather2_guard';
        say $fh "1";
    }
}

sub set($nick, $location, $country) {
    return unless defined($location);

    if (defined($country)) {
        my $sth = $db->prepare('INSERT INTO weather2 (nick, location, country) VALUES (?1, ?2, ?3) ON CONFLICT (nick) DO UPDATE SET location=?2, country=?3;');
        $sth->execute($nick, $location, $country);
    } else {
        my $sth = $db->prepare('INSERT INTO weather2 (nick, location) VALUES (?1, ?2) ON CONFLICT (nick) DO UPDATE SET location=?2;');
        $sth->execute($nick, $location);
    }
}

sub on_log($text) {
    open my $fh, '>>', '/home/chessa/weather.log';
    say $fh $text;
    close $fh;
}

my $w = Chessa::Weather->new(
    wapikey  => Irssi::settings_get_str('weather_api_key'),
    gcapikey => Irssi::settings_get_str('weather_geo_key'),
    irc      => 1,
    on_log   => \&on_log,
);

Irssi::signal_add 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;
    my $metric = undef;

    if ($target !~ /^#/) {
        $target = $nick;
    } else {
        $target .= " $nick:"
    }

    my $trigger;
    ($trigger, $text) = split / /, $text, 2;

    return unless ($trigger =~ s/^\.//);

    my $on_info = sub {
        my ($msg) = @_;

        $server->command("msg $target $msg");
    };

    my $pm_info = sub {
        my ($msg) = @_;

        $server->command("msg $nick $msg");
    };

    my ($location, $country);

    if (!defined($text) || (length($text) == 0)) {
        my $sth = $db->prepare('SELECT location, country FROM weather2 WHERE nick=?;');
        $sth->execute(lc($nick));
        my @row = $sth->fetchrow_array;

        unless (@row < 1) {
            $location = $row[0];
            $country  = $row[1];
        }
    } elsif (defined($text)) {
        if ($text =~ /; ?/) {
            ($location, $country) = split /; ?/, $text, 2;
        } else {
            $location = $text;
        }
    }
    my %triggers = (
        'weather'  => sub { $w->fetch_geocode('conditions', $location, $on_info, undef, $country) },
        'f'        => sub { $w->fetch_geocode('forecast', $location, $on_info, 'today', $country) },
        'f1'       => sub { $w->fetch_geocode('forecast', $location, $on_info, 1, $country) },
        'f3'       => sub { $w->fetch_geocode('forecast', $location, $pm_info, 3, $country) },
        'f7'       => sub { $w->fetch_geocode('forecast', $location, $pm_info, 7, $country) },
        'alerts'   => sub { $w->fetch_geocode('alerts', $location, $pm_info, $country) },
        'location' => sub {
            set(lc($nick), $location, $country);
            $on_info->("Okay, I'll remember you're in $text.");
        },
    );

    if (exists($triggers{$trigger})) {
        if (defined($location)) {
            $triggers{$trigger}->();
        } else {
            if ($trigger eq 'location') {
                $on_info->("\cBError\cB: A location is required. You may specify a country by providing it after a semicolon (;).")
            } else {
                $on_info->("\cBError\cB: Please specify a location via the '.location' command. You may specify a country by providing it after a semicolon (;) for more accurate results.");
            }
        }
    }
};

