
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Irssi;

our $VERSION = '0.5';
our %IRSSI = (
    name        => 'url_title.pl',
    description => 'A URL title announcement script, with plugins',
    license     => 'MIT',
    author      => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house'
);

use Chessa::URI;

sub log($text) {
    open my $fh, '>>', '/home/chessa/url_log.txt';
    say $fh $text;
    close $fh;
}

my @config = qw(
    targets
    nick_blacklist
    twitter_bearer_token
    google_api_key
    twitch_client_id
    site_filters
    filter_targets
);
for my $i (@config) {
    Irssi::settings_add_str('url_title', "url_title_$i", '');
}

my $handler = Chessa::URI->new(
    irc          => 1,
    log          => \&log,
    plugins      => 'Twitter Youtube Twitch NPR',
    Twitter_conf => {
        bearer_token => Irssi::settings_get_str('url_title_twitter_bearer_token'),
    },
    Youtube_conf => {
        key => Irssi::settings_get_str('url_title_google_api_key'),
    },
    GDocs_conf   => {
        key => Irssi::settings_get_str('url_title_google_api_key'),
    },
    Twitch_conf  => {
        id => Irssi::settings_get_str('url_title_twitch_client_id'),
    }
);

sub url_process($target, $on_info, $nick, $text, $errors, $filtering) {
    my @nickblock = split ' ', Irssi::settings_get_str('url_title_nick_blacklist');
    if ((grep {uc($nick) eq uc($_) } @nickblock) || ($text =~ /(notitle)/) || ($text =~ /^[!.\$][a-z]+/i)) {
        return;
    }
    my @channels = split ' ', Irssi::settings_get_str('url_title_filter_targets');
    if ($filtering && grep { uc($target) eq uc($_) } @channels) {
        my $filters = Irssi::settings_get_str('url_title_site_filters');

        if ($handler->filters($text, $filters)) {
            return;
        }
    }


    $handler->parse($text, $on_info, $errors);
}

Irssi::signal_add 'ctcp action' => sub ($server, $text, $nick, $address, $target) {
    my @channels = split ' ', Irssi::settings_get_str('url_title_targets');

    unless (grep { uc($target) eq uc($_)} @channels) {
        return;
    }

    my $on_info = sub {
        my ($msg) = @_;
        $server->command("msg $target $msg");
    };
    url_process($target, $on_info, $nick, $text, 0, 1);
};

Irssi::signal_add 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;
    $target = $nick if (uc($target) eq uc($server->{nick}));

    my $on_info = sub {
        my ($msg) = @_;
        $server->command("msg $target $msg");
    };

    if ($text =~ s/^\.title //i) {
        url_process($target, $on_info, $nick, $text, 1, 0);
    } else {
        my @channels = split ' ', Irssi::settings_get_str('url_title_targets');
        unless (grep { uc($target) eq uc($_)} @channels) {
            return;
        }

        url_process($target, $on_info, $nick, $text, 0, 1);
    }
}