
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Irssi;

our $VERSION = v0.4.0;
our %IRSSI = (
    name        => 'dice.pl',
    description => 'A dicebot script powered by Chessa::Sharp',
    license     => 'MIT',
    authors     => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house'
);

use Chessa::Dice;

my %commands = ();

for my $command (keys %Chessa::Dice::commands) {
    $commands{$command} = \&{"Chessa::Dice::$command"};
}
for my $alias (keys %Chessa::Dice::aliases) {
    $commands{$alias} = $commands{$Chessa::Dice::aliases{$alias}};
}

Irssi::signal_add 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;
    my $comment = '';

    unless ($target =~ /^[#&]/) {
        $target = $nick;
    } else {
        $target .= " $nick:";
    }

    if ($text =~ s/\s*;\s*(.*)//) {
        $comment = $1;
    }

    if ($text =~ s/^\.([a-z]+)/$1/) {
        my ($trigger, $input) = split ' ', $text, 2;
        return unless defined($trigger);
        $trigger = lc($trigger);

        if ($trigger =~ /-/) {
            my $flag = '';
            ($trigger, $flag) = split /-/, $trigger, 2;

            if ($flag eq 'to') {
                my $target2 = '';

                ($target2, $input) = split ' ', $input, 2;
                $target = "$nick,$target2";
            } elsif ($flag eq 'as') {
                my $nick2 = '';

                ($nick2, $input) = split ' ', $input, 2;

                if ($target ne $nick) {
                    $nick = quotemeta $nick;
                    $target =~ s/$nick/$nick2/;
                }
            }
        }

        if (exists($commands{$trigger})) {
            $input = '' unless defined($input);
            my $msg = $commands{$trigger}->($input);

            if (($msg !~ /Error/) && ($comment ne '')) {
                $msg .= " ($comment)";
            }

            $server->command("msg $target $msg");
        }
    }
};
