
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Irssi;

our $VERSION = '0.1';
our %IRSSI = (
    name        => 'bug.pl',
    description => 'Bug tracker integration, with plugins',
    license     => 'MIT',
    author      => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house'
);

use Chessa::Bug;

sub log($text) {
    open my $fh, '>>', '/home/chessa/bug_log.txt';
    say $fh $text;
    close $fh;
}

my @config = qw(
    targets
    nick_blacklist
    default_project
    default_group
);
for my $i (@config) {
    Irssi::settings_add_str('bug', "bug_$i", '');
}

my $handler = Chessa::Bug->new(
    irc          => 1,
    log          => \&log,
    plugins      => 'Adelie',
    conf         => {
        default_project => 'packages',
        default_group   => 'adelie',
    },
);

sub url_process($src, $on_info, $nick, $text, $errors=0) {
    my @nickblock = split ' ', Irssi::settings_get_str('bug_nick_blacklist');
    if ((grep {uc($nick) eq uc($_) } @nickblock) || ($text =~ /nobug/) || ($text =~ /^[!.\$][a-z]+/i)) {
        return;
    }
    # avoid firing on uname output.
    $text =~ s/#\d SMP//;

    $handler->parse($text, $on_info, $src, $errors);
}

Irssi::signal_add 'ctcp action' => sub ($server, $text, $nick, $address, $target) {
    my @channels = split ' ', Irssi::settings_get_str('bug_targets');

    unless (grep { uc($target) eq uc($_)} @channels) {
        return;
    }
    my $src = $target;
    $src =~ s/^#//;

    my $on_info = sub {
        my ($msg) = @_;
        $server->command("msg $target $msg");
    };
    url_process($src, $on_info, $nick, $text);
};

Irssi::signal_add 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;
    return if (uc($target) eq uc($server->{nick}));
    my $src = $target;
    $src =~ s/^#//;

    my $on_info = sub {
        my ($msg) = @_;
        $server->command("msg $target $msg");
    };

    my @channels = split ' ', Irssi::settings_get_str('bug_targets');
    if ($text =~ s/^\.bug //) {
        if ($target eq $src) {
            $src = undef;
        }
        url_process($src, $on_info, $nick, $text, 1);
        return;
    }
    unless (grep { uc($target) eq uc($_)} @channels) {
        return;
    }

    url_process($src, $on_info, $nick, $text);
}