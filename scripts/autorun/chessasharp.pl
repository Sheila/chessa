
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Syntax::Keyword::Try;

use Irssi;

use Chessa::Sharp;

our $VERSION = 0.1;
our %IRSSI = (
    name        => 'chessasharp.pl',
    description => 'Simple test script for testing Chessa::Sharp',
    license     => 'MIT',
    authors     => 'Síle Ekaterin Liszka'
);

sub wrapper($input) {
    my $output;

    my $p = Chessa::Sharp->new();

    try {
        $output = $p->from_string($input);
    } catch {
        $output = "\cBError\cB: $_";
    }

    return $output;
}

Irssi::signal_add 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;

    if ($text =~ s/^\.cs //) {
        my $output = wrapper($text);
        $server->command("msg $target $nick: '$text' --> $output");
    }
};

