use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Math::Random::MT::Auto qw(rand);
use URI::Escape qw(uri_escape);

use Irssi;

our $VERSION = 0.2;
our %IRSSI = (
    name        => 'bot.pl',
    description => 'A set of bot commands for irssi',
    authors     => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house',
    license     => 'MIT',
);

Irssi::settings_add_str('bot', 'bot_owner',  'Sheila');
Irssi::settings_add_str('bot', 'bot_chosen', 'Selected result: %s');

sub cmd_process($server, $trigger, $text, $nick, $target) {
    $target .= " $nick:" if ($server->ischannel($target));

    if ($trigger eq 'give') {
        my ($item, $user) = split / to /, $text, 2;
        $server->command("describe $target sends $item down the bar to $user");
    } elsif ($trigger eq 'say') {
        if (lc($nick) eq lc(Irssi::settings_get_str('bot_owner'))) {
            $target =~ s/ $nick://;
            $server->command("msg $target $text");
        }
    } elsif ($trigger eq 'choose') {
        my @choices = split /, /, $text;
        my $choice = $choices[int(rand(scalar(@choices)))];
        my $str = Irssi::settings_get_str('bot_chosen');
        $server->command("msg $target ".sprintf($str, $choice));
    } elsif ($trigger eq 'target') {
        my ($targets, $choices) = split / /, $text, 2;
        my @choices = split /, /, $choices;
        my @targets;

        foreach (1 .. $targets) {
        push @targets, $choices[int(rand(scalar(@choices)))];
        }
        $server->command("msg $target Targets: ".join(', ', @targets));
    } elsif ($trigger eq 'topicreplace') {
        my $name = undef;
        ($name, $text) = split ' ', $text, 2;
        $text =~ s/^('''.*?'''|""".*?"""|'[^']+'|"[^"]+"|\w+) //;
        my $find = $1;
        $find =~ s/^(['"]{1,3})(.*)\1$/$2/;
        my $replace = $text;
        my $channel = $server->channel_find($name);
        my $topic = $channel->{topic};
        $find = quotemeta $find;

        $topic =~ s/$find/$replace/;

        $server->command("msg $target New topic: $topic");
    }
}

Irssi::signal_add_last 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;
    my ($me, $hey) = ($server->{nick}, undef);

    if ($target =~ /^[#&]/) {
        #$target .= " $nick:"
    } else {
        $target = $nick;
    }
    if ($text =~ /^(?:$me)[,:]? /) {
        $text =~ s/(?:$me)[,:]? //;
        if ($text !~ /^\./) {
            my ($trigger, $str) = split / /, $text, 2;
            cmd_process($server, $trigger, $str, $nick, $target);
        }
    } elsif ($text =~ /^\.[a-z]+/) {
        $text =~ s/^\.//;
        my ($trigger, $str) = split / /, $text, 2;
        cmd_process($server, $trigger, $str, $nick, $target);
    }
};
