
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Irssi;

our $VERSION = 0.1;
our %IRSSI = (
    name        => 'database.pl',
    description => 'An irssi interface into Chessa::Database databases',
    author      => 'Síle Ekaterin Liszka',
    contact     => 'sheila@vulpine.house',
);

Irssi::settings_add_str('database', 'database_load_list', '');

sub log($msg) {
    open my $fh, '>>', Irssi::get_irssi_dir . '/../db_error.txt' or die "Can't open error log: $!\n";
    say $fh $msg;
    close $fh;
}

use Chessa::Database;

my $db = Chessa::Database->new(
    irc     => 1,
    log     => \&log,
    root    => Irssi::get_irssi_dir,
    plugins => Irssi::settings_get_str('database_load_list'),
);

Irssi::signal_add 'event privmsg' => sub ($server, $data, $nick, $address) {
    my ($target, $text) = split / :/, $data, 2;
    $target = $nick if (uc($target) eq uc($server->{nick}));
    my $network = $server->{chatnet};

    return unless $text =~ s/^\.([a-z]+)/$1/;
    my $trigger = '';

    ($trigger, $text) = split ' ', $text, 2;
    my @plugins = split ' ', Irssi::settings_get_str('database_load_list');

    return unless defined($trigger) && grep { lc $_ eq lc $trigger } @plugins;
    my $name = '';
    my $output = '';
    my $func = '';

    my $on_info = sub {
        my ($msg) = $_[0];
        $msg =~ s/^Error/\cBError\cO/;
        $server->command("msg $target $_[0]");
    };

    # some clients insert whitespace after tabcompletion.
    $text =~ s/\s*$// if defined($text);

    if (!defined($text)) {
        $func = 'find';
        $name = $nick;
    } else {
        if ($text =~ s/^(find|set|del) //) {
            $func = $1;
            ($name, $text) = split ' ', $text, 2;
            unless (length($text)) {
                $text = $name;
                $name = $nick;
            }
        } elsif ($text =~ /^help/i) {
            $func = 'help';
            $name = 'lol';
        } elsif (length($text) > 0) {
            $func = 'find';
            $name = $text;
        }
    }

    return if (($func =~ /^(set|del)$/) && ($name ne $nick));

    $on_info->($db->call($trigger, $func, $name, $network, $text));
};
