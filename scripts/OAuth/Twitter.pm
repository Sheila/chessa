package OAuth::Twitter;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use HTTP::Request;
use MIME::Base64 qw(encode_base64url decode_base64url);
use JSON::MaybeXS;
use Try::Tiny;

=head1 NAME

C<OAuth::Twitter> - OAuth 2.0 for Twitter's application-only API

=head1 SYNOPSIS

 use OAuth::Twitter;

 # these are both examples provided by Twitter's application-only
 # authentication API. do not use them in real code.
 my $twapp = OAuth::Twitter->new(
    consumer_key    => 'xvz1evFS4wEEPTGEFPHBog',
    consumer_secret => 'L8qq9PZyRg6ieKGEKhZolGC0vJWLw8iEJ88DRdyOg'
 );

 my ($request, $callback) = $twapp->get_bearer_token;

 # Use whatever mechanism you wish to send the request, preferably one
 # that can use a callback; you will need to invoke the provided
 # callback in order to build requests.
 ...

 my ($request) = $twapp->do_request(
    uri => 'https://api.twitter.com/1.1/statuses/user_timeline.json?count=100&screen_name=twitterapi'
 );

 # Use whatever mechanism you wish to send the request. Note that you
 # will have to process the response (in JSON::MaybeXS) yourself.

=head1 DESCRIPTION

This module provides a minimal API for working with Twitter's
application-only authentication API. You will need to register with
Twitter and create an application on their developer website in order
to use this module.

=head1 FUNCTIONS

=head2 new

Generates a new OAuth::Twitter object. Requires both C<consumer_key> and
C<consumer_secret> keyword arguments. Both of these are provided by Twitter
through their developer portal (L<http://dev.twitter.com>).

=cut

sub new($class, %args) {
    my $self = {};

    $self->{consumer_key} = delete $args{consumer_key} or croak "consumer key required";
    $self->{consumer_sec} = delete $args{consumer_secret} or croak "consumer secret required";

    return bless $self, $class || ref $class;
}

=head2 has_bearer_token

Returns truthiness if this object bears a bearer token, falsiness otherwise.

=cut

sub has_bearer_token($self) {
    return (exists($self->{bearer}) and defined($self->{bearer}))
}

=head2 get_bearer_token

This method generates an C<HTTP::Request> object for use with your favourite
HTTP agent, and a callback to be called upon receiving an C<HTTP::Response>
object from that agent. If a token couldn't be acquired, it croaks.

Neither C<invalidate_bearer_token> nor C<do_request> may be used without first
invoking this function.

Note that if your HTTP agent doesn't pull its URI from the request object, you
may need to instruct it to use SSL with C<api.twitter.com>.

=cut

sub get_bearer_token($self) {
    if (exists $self->{bearer}) {
        warn "Attempted to request a new token without invalidating previous token";
        return (undef, undef);
    }

    my ($key, $secret) = ($self->{consumer_key}, $self->{consumer_sec});

    my $headers = [
        'Authorization' => 'Basic ' . encode_base64url("$key:$secret"),
        'Content-Type'  => 'application/x-www-form-urlencoded;charset=UTF-8'
    ];

    my $req = HTTP::Request->new('POST', 'https://api.twitter.com/oauth2/token', $headers);

    $req->content('grant_type=client_credentials');

    my $cb = sub ($response) {
        my $json;

        if ($response->code != 200) {
            if (length($response->decoded_content) == 0) {
                croak "Couldn't acquire token: " . $response->status_line;
            }

            try {
                $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);
            } catch {
                croak "Failed to decode JSON (probably not JSON?): $_";
            }

            croak "Couldn't acquire token: $json->{errors}{message}";
        }

        $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

        $self->{bearer} = $json->{access_token};
    };

    return ($req, $cb);
}

=head1 invalidate_bearer_token

This method invalidates the bearer token associated with this object. It
returns a C<HTTP::Request> object which must be processed by your HTTP
agent.

Note that if your HTTP agent doesn't pull its URI from the request object, you
may need to instruct it to use SSL with C<api.twitter.com>.

=cut

sub invalidate_bearer_token($self) {
    my $key    = $self->{consumer_key};
    my $secret = $self->{consumer_sec};

    unless (exists $self->{bearer}) {
        warn "Attempted to invalidate nonexistent token";
        return undef;
    }

    my $headers = [
        'Authorization' => 'Basic ' . encode_base64url("$key:$secret"),
        'Content-Type'  => 'application/x-www-form-urlencoded;charset=UTF-8'
    ];

    my $req = HTTP::Request->new('POST', 'https://api.twitter.com/oath2/invalidate_token', $headers);

    $req->content('access_token=' . $self->{bearer});

    delete $self->{bearer};

    return ($req);
}

=head2 c<do_request>

This method takes C<uri> and, optionally, c<method> keyword arguments, and
generates a C<HTTP::Request> object for use with your HTTP agent of choice.

=cut

sub do_request($self, %args) {
    unless (exists $self->{bearer}) {
        warn "Can't process a request without a bearer token";
        return undef;
    }

    unless (exists $args{uri}) {
        warn "Cowardly refusing to fetch without a URI";
        return undef;
    }

    $args{method} = 'GET' unless (defined($args{method}));

    my $headers = [
        'Authorization' => 'Bearer ' . $self->{bearer}
    ];
    my $req = HTTP::Request->new($args{method}, $args{uri}, $headers);

    return ($req);
}

=head1 SEE ALSO

=over 2

=item *

L<https://dev.twitter.com> - Twitter Developer portal

=back

=head1 AUTHOR

Síle Ekaterin Liszka <sheila@vulpine.house>

=head1 ACKNOWLEDGEMENTS

This module makes use of C<HTTP::Request>, C<MIME::Base64>, C<JSON::MaybeXS>, and C<Try::Tiny>.

=cut

1;
