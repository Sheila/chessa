package Chessa::Database;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use DBI;

use Module::PluginFinder;
use Syntax::Keyword::Try;

sub new($class, %args) {
    my $self = {};

    $self->{irc}  = delete $args{irc} // 0;
    $self->{log}  = delete $args{log} // sub {};
    $self->{root} = delete $args{root} // croak "Need path for database storage.";

    # find our handlers...
    my $finder = Module::PluginFinder->new(
        search_path => 'Chessa::Database',
        typefunc    => 'name'
    );
    $self->{finder} = $finder;
    $self->{plugins} = {};
    $self->{plugin_names} = [];
    $self->{msg} = sub { $self->message(@_) };
    $self->{error} = sub ($on_info, $text) {
        my $msg = $self->message('Error', $text);
        $self->{log}($text);
        $on_info->($msg);
    };

    my @plugins = split ' ', $args{plugins};

    croak "Must load at least one plugin" unless @plugins;

    for my $plugin (@plugins) {
        load_module($self, $plugin, $args{"${plugin}_conf"});
    }

    $self->{init} = 1;

    return bless $self, $class || ref $class;
}

sub message($self, $input, $heading, $text) {
    my ($bold, $end) = ('', '');

    if ($self->{irc}) {
        $bold = "\cB";
        $end  = "\cO";
    }

    return "${bold}${heading}${end}: $text";
}

sub load_module($self, $module, $conf) {
    my $loaded = undef;

    unless (exists($self->{init})) {
        $self->{finder}->rescan;
    }

    if (defined($conf) && ref($conf) eq 'HASH') {
        $conf->{irc} = $self->{irc};
    } else {
        $conf = {irc => $self->{irc}};
    }

    $self->{log}("Attempting to load $module plugin...");

    my $db = DBI->connect('dbi:SQLite:dbname=' . $self->{root} . '/' . $module, '', '');

    try {
        $loaded = $self->{finder}->construct($module, $conf, $db, $self->{msg}, $self->{error});
    } catch {
        $self->{log}("Couldn't load $module plugin: $@");

        return "Failed to load $module plugin: $@";
    }

    unless (-e $self->{root} . "/${module}_guard") {
        open my $fh, $self->{root} . "/${module}_guard";
        say $fh 1;
        close $fh;
        $loaded->init_database;
    }

    $self->{plugins}{lc $module} = $loaded;
    push @{$self->{plugin_names}}, lc $module;

    return "Loaded $module plugin.";
}

sub get_modules($self) {
    return join(', ', $self->{plugin_names});
}

sub call($self, $module, $func, @params) {
    croak "No such module: $module" unless exists($self->{plugins}{lc $module});
    my $plugin = $self->{plugins}{lc $module};

    croak "Module '$module': $func method missing" unless ($plugin->can($func));
    return $plugin->$func(@params);
}

# A well-fed woof is a happy woof.
0xfeedbeef;
