package Chessa::Weather;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use constant QUERY_ERROR => 'Please provide a query string.';

use Carp qw(croak);
use URI;
use URI::Escape;
use Encode qw(encode);
use POSIX qw(round strftime);

use Net::Async::HTTP;
use IO::Async::Loop::Glib;
use JSON::MaybeXS;
use DateTime::TimeZone;

our $loop = IO::Async::Loop::Glib->new();
my $http = Net::Async::HTTP->new(
    user_agent => "Mozilla/5.0 (Linux x86_64) Chessa::Weather 0.5.0",
);

$loop->add($http);

sub new($class, %args) {
    my $self = {};

    $self->{wapikey} = delete $args{wapikey} or croak "an OpenWeatherMap API key is required";
    $self->{gcapikey} = delete $args{gcapikey} or croak "a MapBox API key is required";

    $self->{on_log} = sub {} unless defined($self->{on_log});
    $self->{wapi} = qq[https://api.openweathermap.org/data/3.0/onecall];
    $self->{gcapi} = qq{https://api.mapbox.com/geocoding/v5/mapbox.places/};

    return bless $self, $class || ref($class);
}

sub log($self, $msg, $on_info, $error=0) {
    $self->{on_log}($msg);
    $on_info->($self->error($msg)) if $error;
}

sub msg($self, $title, $text) {
    if ($self->{irc}) {
        return "\cB$title\cB: $text";
    } else {
        return "$title: $text";
    }
}

sub error($self, $text) {
    return $self->msg('Error', $text);
}

sub f2c($temp) {
    return round(100 * (5 / 9) * ($temp - 32)) / 100;
}
sub c2f($temp) {
    return round(100 * (((9 / 5) * $temp) + 32)) / 100;
}

sub temp($temp, $unit) {
    my $out = '';

    if ($unit eq 'f') {
        $out = $temp . 'F/' . f2c($temp) . 'C';
    } elsif ($unit eq 'c') {
        $out = c2f($temp) . 'F/' . $temp . 'C';
    }

    return $out;
}

sub mm2in($num) {
    return round($num) * 10 / 2.54;
}

sub deg2dir($deg) {
    my @dir = (qw(N NNE NE ENE E ESE SE SSE S SSW SW WSW W WNW NW NNW N));

    $deg = ($deg % 360) / 22.5 - 1;

    return $dir[$deg];
}

sub ms2mph($ms) {
    return round(100 * $ms * 3600 / 1609.3) / 100;
}
sub mph2ms($mph) {
    return round(100 * $mph * 1609.3 / 3600) / 100;
}

sub speed($spd, $unit) {
    my $out = '';
    if ($unit eq 'ms') {
        $out = ms2mph($spd) . 'mph/' . $spd * 3.6 . 'kmph'
    } elsif ($unit eq 'mph') {
        $out = $spd . 'mph/' . mph2ms($spd) * 3.6 . 'kmph'
    }

    return $out;
}

sub timezone($timezone) {
    my ($hours, $minutes);
    $hours = int($timezone / 60 / 60);
    $minutes = int($timezone / 60 % 60);
    return sprintf '%0' . ($hours < 0 ? '3' : '2') . 'd%02d', $hours, $minutes;
}

sub fetch_geocode($self, $type, $query, $on_info, $range, $country) {
    my $uri = $self->{gcapi} . uri_escape($query) . '.json?limit=1&access_token=' . $self->{gcapikey};

    $uri .= '&country=' . $country if (defined($country));

    $http->do_request(
        uri         => $uri,
        on_response => sub ($response) {
            $self->log("Fetched geocode for $query", $on_info);
            $self->parse_geocode($type, $response, $on_info, $range);
        }, on_error => sub ($msg) {
            $self->log("Failed to fetch geocode for $query: $msg", $on_info, 1);
        }
    );
}

sub parse_geocode ($self, $type, $response, $on_info, $range) {
    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

    if (defined $json->{message}) {
        $on_info->($self->error("Unable to fetch location data: " . $json->{message}));
        return;
    }
    my $lat = $json->{features}[0]{center}[1];
    my $lon = $json->{features}[0]{center}[0];
    my $name = $json->{features}[0]{place_name};

    if ($name =~ /United States$/) {
        my @fragments = split /, /, $name;
        my $state = $fragments[-2];
        my $city = $fragments[-3];
        $state =~ s/ \d{5}(?:-\d+)?//;
        if ($fragments[0] =~ /\(.*?\)/) {
            $name = "$fragments[0], $city, $state";
        } else {
            $name = "$city, $state";
        }
    } elsif ($name =~ /Canada$/) {
        my @fragments = split /, /, $name;
        if ($fragments[0] =~ /^\w\d\w \d\w\d$/) {
            shift @fragments;
            $name = join ', ', @fragments;
        }
    } else {
        my @fragments = split /, /, $name;
        if (@fragments > 2) { $name = "$fragments[0], $fragments[-1]"; }
    }
    my ($func, @exclude) = (undef, (qw[minutely hourly]));

    if ($type eq 'conditions') {
        push @exclude, ['daily', 'alerts'];
        $func = \&fetch_conditions;
    } elsif ($type eq 'forecast') {
        push @exclude, ['currently', 'alerts'];
        $func = \&fetch_forecast;
    } elsif ($type eq 'alerts') {
        push @exclude, ['currently', 'daily'];
        $func = \&fetch_alerts;
    }

    $http->do_request(
        uri         => $self->{wapi} . '?appid=' .$self->{wapikey} . '&lat=' . $lat . '&lon=' . $lon .  '&units=metric&exclude=' . join(',', @exclude),
        on_response => sub ($response) {
            $self->log("Fetched $type for $json->{features}[0]{place_name} ($lat,$lon)", $on_info);
            $func->($self, $response, $name, $on_info, $range);
        }, on_error => sub ($msg) {
            self->log("Failed to fetch $type for $json->{features}[0]{place_name}: $msg", $on_info, 1);
        }
    );
}

sub fetch_conditions ($self, $response, $name, $on_info, $range) {
    my $json = JSON::MaybeXS->new->decode($response->decoded_content);

    if (exists $json->{error}) {
        $on_info->($self->error("Unable to fetch weather conditions: " . $json->{error}));
        return;
    }

    my $dt = DateTime->from_epoch(
        epoch     => $json->{current}{dt},
        time_zone => timezone($json->{timezone_offset}),
    );
    my $sunset = DateTime->from_epoch(
        epoch     => $json->{current}{sunset},
        time_zone => timezone($json->{timezone_offset}),
    );

    my %w = (
        city   => $name,
        temp   => temp($json->{current}{temp}, 'c'),
        like   => temp($json->{current}{feels_like}, 'c'),
        wind   => deg2dir($json->{current}{wind_deg}) . ' @ ' . speed($json->{current}{wind_speed}, 'ms'),
        humid  => $json->{current}{humidity} . '%',
        press  => $json->{current}{pressure} . 'hPa',
        time   => $dt->datetime(' ') . ' ' . $dt->time_zone_short_name,
        sunset => $sunset->strftime('%I:%M %p'),
    );
    if (exists($json->{current}{wind_gust})) {
        $w{wind} .= ', gusts up to ' . speed($json->{current}{wind_gust}, 'ms');
    }
    if (exists($json->{current}{rain})) {
        my $p = $json->{current}{rain}{'1h'};
        $w{rain} = mm2in($p) . "in/${p}mm";
    }
    if (exists($json->{current}{snow})) {
        my $p = $json->{current}{snow}{'1h'};
        $w{snow} = mm2in($p) . "in/${p}mm";
    }
    my $weather = $json->{current}{weather}[0];
    $w{cond} = $weather->{description};

    my @message = (
        $self->msg($w{city}, $w{cond}),
        $self->msg('Temp', $w{temp}),
        $self->msg('Feels Like', $w{like}),
        $self->msg('Humidity', $w{humid}),
        $self->msg('Wind', $w{wind}),
        $self->msg('Pressure', $w{press}),
    );
    push @message, $self->msg('Rain', $w{rain}) if exists($w{rain});
    push @message, $self->msg('Snow', $w{snow}) if exists($w{snow});
    push @message, $self->msg('Sunset', $w{sunset});
    push @message, $self->msg('Observed', $w{time});

    $on_info->(join ' ', @message);
}

sub fetch_forecast($self, $response, $name, $on_info, $range) {
    my $json = JSON::MaybeXS->new->decode($response->decoded_content);

    if (exists $json->{error}) {
        $on_info->($self->error("Unable to fetch weather conditions: " . $json->{error}));
        return;
    }
    my @reports = ();

    for my $day (@{ $json->{daily} }) {
        my $dt = DateTime->from_epoch(
            epoch     => $day->{dt},
            time_zone => timezone($json->{timezone_offset}),
        );
        my $sunset = DateTime->from_epoch(
            epoch     => $day->{sunset},
            time_zone => timezone($json->{timezone_offset}),
        );
        my %w = (
            cond   => $day->{weather}[0]{description},
            low    => temp($day->{temp}{min}, 'c'),
            high   => temp($day->{temp}{max}, 'c'),
            wind   => deg2dir($day->{wind_deg}) . ' @ ' . speed($day->{wind_speed}, 'ms'),
            humid  => $day->{humidity} . '%',
            press  => $day->{pressure} . 'hPa',
            time   => $dt->strftime("%A, %d %B"),
            sunset => $sunset->strftime("%I:%M %p"),
        );
        if (exists($day->{wind_gust})) {
            $w{wind} .= ', gusting up to ' . speed($day->{wind_gust}, 'ms');
        }
        $w{time} =~ s/0(\d)/$1/;
        push @reports, join ' ', (
            $self->msg($w{time}, $w{cond}),
            $self->msg('High', $w{high}),
            $self->msg('Low', $w{low}),
            $self->msg('Humidity', $w{humid}),
            $self->msg('Pressure', $w{press}),
            $self->msg('Wind', $w{wind}),
            $self->msg('Sunset', $w{sunset}),
        );
    }

    my ($start, $stop) = (0, $range);
    if ($range eq 'today') {
        $stop = 0;
        $on_info->($self->msg("Today's forecast", $name));
    } else {
        $start = 1;
        $on_info->($self->msg($range == 1 ? "Tomorrow's forecast" : $range . '-day forecast', $name));
    }
    for my $i ($start .. $stop) {
        $on_info->($reports[$i]);
    }
}

sub format_alert($self, $timezone, $name, $alert) {
    my $start = DateTime->from_epoch(
        epoch     => $alert->{start},
        time_zone => $timezone,
    );
    my $stop = DateTime->from_epoch(
        epoch     => $alert->{end},
        time_zone => $timezone,
    );
    my $ret = 'From ' . $start->strftime('%F %R') . ' to ' .
        $stop->strftime('%F %R');
    $ret .= ', via ' . $alert->{sender_name} . ': ' .
        $alert->{event} . '. ' . $self->msg('Description', $alert->{description});

    return $ret;
}

sub fetch_alerts($self, $response, $name, $on_info, $range) {
    my $json = JSON::MaybeXS->new->decode($response->decoded_content);

    if (exists $json->{error}) {
        $on_info->($self->error("Unable to fetch weather conditions: " . $json->{error}));
        return;
    }

    if (exists($json->{alerts}) && (@{$json->{alerts}} > 0)) {
        if (@{$json->{alerts}} == 1) {
            my $alert = $json->{alerts}[0];
            $on_info->($self->format_alert(
                timezone($json->{timezone_offset}), $name, $alert
            ));
        } else {
            $on_info->($self->msg('Alerts for', $name));
            for my $alert (@{$json->{alerts}}) {
                $on_info->($self->format_alert(
                    timezone($json->{timezone_offset}), $name, $alert
                ));
            }
        }
    } else {
        $on_info->("No alerts found for $name.");
    }
}

# A well-fed woof is a happy woof.
0xfeedbeef;
