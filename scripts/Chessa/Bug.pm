package Chessa::Bug;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use constant {
    USER_AGENT => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
};

use IO::Async::Loop::Glib;
use Net::Async::HTTP;
use Module::PluginFinder;
use Syntax::Keyword::Try;
use Syntax::Keyword::Match;

sub new($class, %args) {
    my $self = {};

    $self->{irc}  = delete $args{irc} // 0;
    $self->{log}  = delete $args{log} // sub {};
    $self->{conf} = delete $args{conf} or die "missing Chessa::Bug config";

    # set up the web client.
    $self->{http} = Net::Async::HTTP->new(
        user_agent    => USER_AGENT,
        timeout       => 30,
        stall_timeout => 30,
        SSL_version   => 'TLSv12:!SSLv23:!SSLv2:!SSLv3',
        headers       => {
            'Accept-Language' => 'en-US,en;q=0.5',
        },
    );
    $self->{loop} = IO::Async::Loop::Glib->new();
    $self->{loop}->add($self->{http});

    # find our handlers...
    my $finder = Module::PluginFinder->new(
        search_path => 'Chessa::Bug',
        typefunc    => 'name'
    );
    $self->{finder} = $finder;
    $self->{plugins} = [];
    $self->{finder}->rescan;

    my @plugins = $finder->modules;

    for my $plugin (@plugins) {
        load_module($self, $plugin, $args{"${plugin}_conf"});
    }

    $self->{init} = 1;

    return bless $self, $class || ref $class;
}

sub load_module($self, $module, $conf) {
    my $loaded = undef;

    if (defined($conf) && ref($conf) eq 'HASH') {
        $conf->{irc} = $self->{irc};
    } else {
        $conf = {irc => $self->{irc}};
    }

    $self->{log}("Attempting to load $module plugin...");
    
    my @frags = split /::/, $module;
    $module = $frags[-1];

    try {
        $loaded = $self->{finder}->construct($module, $self->{http}, $self->{log}, sub { $self->message(@_) }, $conf);
    } catch {
        $self->{log}("Couldn't load $module plugin: $@");

        return "Failed to load $module plugin: $@";
    }

    push @{$self->{plugins}}, $loaded;

    $self->{log}("Loaded $module plugin.");
}

sub message($self, $header, $text) {
    my $bold = $self->{irc} ? "\cB" : '';
    my $end  = $self->{irc} ? "\cO" : '';

    return "$bold$header$end: $text";
}

sub parse($self, $input, $on_info, $src, $errors) {
    my @words = split ' ', $input;
    my @uris = ();

    for my $word (@words) {
        my ($project, $num, $type, $sigil, $group, $trash);
        $word =~ s/[](){}[]//g;
        match ($word : =~) {
            case (/#(\d+)\b/) {
                $num = $1;
                $type = 'issues';
                $sigil = quotemeta '#';
            }
            case (/!(\d+)\b/) {
                $num = $1;
                $type = 'merge_requests';
                $sigil = quotemeta '!';
            }
            case (/\$\$(\d+)\b/) {
                $num = $1;
                $type = 'snippets';
                $sigil = quotemeta '$';
            }
            case (/@([a-fA-F0-9]{7,})/) {
                $num = $1;
                $type = 'commits';
                $sigil = quotemeta '@';
            }
            default {
                next;
            }
        }
        next if ($word =~ /(\$(\d+\.\d*|\d)|^#\d)$/);
        ($project, $trash) = split /$sigil/, $word;
        if ($type eq 'commits') {
            $num =~ s/^([a-fA-F0-9]+).*/$1/;
        } else {
            $num =~ s/^(\d+).*/$1/;
        }

        if (length($project)) {
            if ($project =~ s!^(.*?)/!!) {
                $group = $1;
            } else {
                $group = $self->{conf}{default_group};
            }
        } elsif ($type eq 'snippets') {
            # lol
        } else {
            $project = $self->{conf}{default_project};
            $group   = $self->{conf}{default_group};
        }
        $project = join '/', $group, $project unless (!length($project));

        if (($project !~ /:/) && defined($src)) {
            $project = "$src:$project";
        }

        $self->{log}("Found $project $type $num\n");
        push @uris, [$project, $num, $type];
    }

    while (scalar(@uris) > 0) {
        my $uri = shift @uris;
        for my $plugin (@{$self->{plugins}}) {
            if ($plugin->wants($uri->[0])) {
                $uri->[0] =~ s/^.*?://g;
                $self->{log}("Handing off to " . $plugin->name);
                $plugin->handle($on_info, $uri, $errors);
                last;
            }
        }
    }
}

# A well-fed woof is a happy woof.
0xfeedbeef;
