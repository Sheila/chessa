package Chessa::URI;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use constant {
    USER_AGENT => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36",
};

use URI;
use URI::Find::Simple qw(list_uris);
use IO::Async::Loop::Glib;
use Net::Async::HTTP;
use Module::PluginFinder;
use Syntax::Keyword::Try;

sub new ($class, %args) {
    my $self = {};

    $self->{irc}    = delete $args{irc} // 0;
    $self->{log}    = delete $args{log} // sub {};

    # set up the web client.
    $self->{http} = Net::Async::HTTP->new(
        user_agent    => USER_AGENT,
        timeout       => 30,
        stall_timeout => 30,
        SSL_version   => 'TLSv12:!SSLv23:!SSLv2:!SSLv3',
        headers       => {
            'Accept-Language' => 'en-US,en;q=0.5',
        },
    );
    $self->{loop} = IO::Async::Loop::Glib->new();
    $self->{loop}->add($self->{http});

    # find our handlers...
    my $finder = Module::PluginFinder->new(
        search_path => 'Chessa::URI',
        typefunc    => 'name'
    );
    $self->{finder} = $finder;
    $self->{plugins} = [];

    my @plugins = split ' ', $args{plugins};
    # the Default handler must be last.
    push @plugins, 'Default';
    # The Ignore handler must be first.
    unshift @plugins, 'Ignore';

    for my $plugin (@plugins) {
        load_module($self, $plugin, $args{"${plugin}_conf"});
    }

    $self->{init} = 1;

    return bless $self, $class || ref $class;
}

sub log($self, $message, $on_info, $errors=0) {
    $self->{log}($message);
    $on_info->($message) if $errors;
}

sub load_module ($self, $module, $conf) {
    my $loaded = undef;

    unless (exists($self->{init})) {
        $self->{finder}->rescan;
    }

    if (defined($conf) && ref($conf) eq 'HASH') {
        $conf->{irc} = $self->{irc};
    } else {
        $conf = {irc => $self->{irc}};
    }

    $self->{log}("Attempting to load $module plugin...");

    try {
        $loaded = $self->{finder}->construct(
            $module, $self->{http},
            sub ($message, $on_info, $errors=0) {
                $self->log($message, $on_info, $errors)
            }, sub {
                $self->message(@_)
            }, $conf);
    } catch {
        $self->{log}("Couldn't load $module plugin: $@");

        return "Failed to load $module plugin: $@";
    }

    push @{$self->{plugins}}, $loaded;

    return "Loaded $module plugin.";
}

sub message($self, $header, $text) {
    my $bold = $self->{irc} ? "\cB" : '';
    my $end  = $self->{irc} ? "\cO" : '';

    return "$bold$header$end: $text";
}

sub parse($self, $input, $on_info, $errors) {
    $self->{log}("Parsing URIs...");

    my @uris = list_uris($input);

    return if @uris > 2;

    while (scalar(@uris) > 0) {
        my $uri = shift @uris;
        $uri = URI->new($uri);

        next unless $uri->scheme =~ /^https?$/;

        $self->{log}("Found URI: $uri");
        for my $plugin (@{$self->{plugins}}) {
            if ($plugin->wants($uri)) {
                $self->{log}('Passed URI to ' . $plugin->name . ' plugin...');
                $plugin->handle($on_info, $uri, $errors);
                last;
            }
        }
    }
}

sub filters($self, $input, $filters) {
    my @uris = list_uris($input);
    my @filters = split ' ', $filters;

    foreach my $uri (@uris) {
        foreach my $filter (@filters) {
            $uri = URI->new($uri);
            my @uri_frags = split /\./, $uri->authority;
            my @filter_frags = split /\./, $filter;
            while (@uri_frags > @filter_frags) {
                shift @uri_frags;
            }
            $uri = join '.', @uri_frags;
            if ((@uri_frags == @filter_frags) && (lc($filter) eq lc($uri))) {
                return 1;
            }
        }
    }
    return 0;
}

# A well-fed woof is a happy woof.
0xfeedbeef;
