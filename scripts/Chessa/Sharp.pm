package Chessa::Sharp;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Chessa::Number;

use Carp qw(croak);
use POSIX;
use List::Util qw(sum max min);

use Math::Random::MT::Auto qw(rand);

use base qw(Parser::MGC);

sub new($class, %args) {
    my $self = $class->SUPER::new( accept_0o_oct => 1 );
    $self->{rand} = $args{rand} || \&rand;
    return $self
}

sub parse($self) {
    $self->parse_expr->to_string;
}

sub parse_expr($self) {
    my $val = $self->parse_shift;

    1 while $self->any_of(
        sub { $self->expect('&'); $self->commit; $val = $val->band($self->parse_shift); 1 },
        sub { $self->expect('^'); $self->commit; $val = $val->bxor($self->parse_shift); 1 },
        sub { $self->expect('|'); $self->commit; $val = $val->bor($self->parse_shift); 1 },
        sub { 0 }
    );

    return $val;
}

sub parse_shift($self) {
    my $val = $self->parse_term;

    1 while $self->any_of(
        sub { $self->expect('<<'); $self->commit; $val = $val->lsh($self->parse_term); 1 },
        sub { $self->expect('>>'); $self->commit; $val = $val->rsh($self->parse_term); 1 },
        sub { 0 }
    );

    return $val;
}

sub parse_term($self) {
    my $val = $self->parse_factor;

    1 while $self->any_of(
        sub { $self->expect('+'); $self->commit; $val = $val->add($self->parse_factor); 1 },
        sub { $self->expect('-'); $self->commit; $val = $val->sub_($self->parse_factor); 1 },
        sub { 0 }
    );

    return $val;
}

sub _roll($self, $dice, $sides) {
    croak "too many dice rolled" unless ($dice->{num} < 1000);
    croak "too many sides" unless ($sides->{num} <= 100);
    return Chessa::Number->new(num => sum 0, map int($self->{rand}->($sides->{num})) + 1, 1 .. $dice->{num} )
}

sub parse_factor($self) {
    my $val = $self->parse_exp;

    1 while $self->any_of(
        sub { $self->expect('*'); $self->commit; $val = $val->mul($self->parse_exp); 1 },
        sub { $self->expect('%'); $self->commit; $val = $val->mod($self->parse_exp); 1 },
        sub { $self->expect('/'); $self->commit; $val = $val->div($self->parse_exp); 1 },
        sub { $self->expect('d'); $self->commit; $val = $self->_roll($val, $self->parse_exp); 1 },
        sub { 0 }
    );

    return $val;
}

sub parse_exp($self) {
    my $val = $self->parse_unary;

    1 while $self->any_of(
        sub { $self->expect('**'); $self->commit; $val = $val->exp($self->parse_unary); 1 },
        sub { 0 }
    );

    return $val;
}

sub parse_unary($self) {
    my $val;
    
    if ($self->any_of(
       sub { $self->expect('+'); $self->commit; $val = $self->parse_parens->abs; 1 },
       sub { $self->expect('-'); $self->commit; $val = $self->parse_parens->neg; 1 },
       sub { $self->expect('~'); $self->commit; $val = $self->parse_parens->bneg; 1 },
       sub { 0 }
    )) {
        return $val
    }

    return $self->parse_parens;
}

sub parse_parens($self) {
    $self->any_of(
        sub { $self->scope_of('(', sub { $self->commit; $self->parse_expr }, ')') },
        sub { $self->parse_atom }
    );
}

sub parse_atom($self) {
    $self->any_of(
        sub { $self->parse_func },
        sub { $self->parse_symbol }
    );
}

sub _memoize($memo, $func) {
    sub recur($n) {
        my $ret = $memo->[$n];
        unless (defined($ret)) {
            $ret = $func->(\&recur, $n);
            $memo->[$n] = $ret;
        }
        return $ret;
    }

    return \&recur;
}

sub _fact($recur, $n) {
    croak "be reasonable\n" if ($n > 1000);

    return $n * $recur->($n - 1)
}

my $fact = _memoize([1, 1], \&_fact);

sub maxim(@objs) {
    my @nums = ();

    for my $x (@objs) {
        push @nums, $x->{num};
    }

    return max(@nums);
}
sub minim(@objs) {
    my @nums = ();

    for my $x (@objs) {
        push @nums, $x->{num};
    }

    return min(@nums);
}
sub stage($stage) {
    $stage = $stage->{num};
    if ($stage > 0) {
        return 1 + ($stage / 5)
    } elsif ($stage < 0) {
        return 1 + ($stage / 10)
    } else {
        return 1
    }
}

my %functions = (
    'int'   => { args => 1, body => sub { Chessa::Number->new(num => int($_[0]->{num}), base => 10) }},
    'hex'   => { args => 1, body => sub { Chessa::Number->new(num => int($_[0]->{num}), base => 16) }},
    'oct'   => { args => 1, body => sub { Chessa::Number->new(num => int($_[0]->{num}), base => 8) }},
    'bin'   => { args => 1, body => sub { Chessa::Number->new(num => int($_[0]->{num}), base => 2) }},
    'atan2' => { args => 2, body => sub { Chessa::Number->new(num => atan2($_[0]->{num}, $_[1]->{num}) ) } },
    'cos'   => { args => 1, body => sub { Chessa::Number->new(num => cos($_[0]->{num})) } },
    'acos'  => { args => 1, body => sub { Chessa::Number->new(num => POSIX::acos($_[0]->{num})) }},
    'exp'   => { args => 1, body => sub { Chessa::Number->new(num => exp($_[0]->{num})) } },
    'log'   => { args => 1, body => sub { Chessa::Number->new(num => log($_[0]->{num})) } },
    'logn'  => { args => 2, body => sub { Chessa::Number->new(num => log($_[0]->{num}) / log($_[1]->{num})) } },
    'log10' => { args => 1, body => sub { Chessa::Number->new(num => log($_[0]->{num}) / log(10)) } },
    'sin'   => { args => 1, body => sub { Chessa::Number->new(num => sin($_[0]->{num})) } },
    'asin'  => { args => 1, body => sub { Chessa::Number->new(num => atan2($_[0]->{num}, sqrt(1 - $_[0]->{num} * $_[0]->{num}))) } },
    'sqrt'  => { args => 1, body => sub { Chessa::Number->new(num => sqrt($_[0]->{num})) } },
    'f2c'   => { args => 1, body => sub { Chessa::Number->new(num => (5 / 9) * ($_[0]->{num} - 32)) } },
    'c2f'   => { args => 1, body => sub { Chessa::Number->new(num => ((9 / 5) * $_[0]->{num}) + 32) } },
    'floor' => { args => 1, body => sub { Chessa::Number->new(num => POSIX::floor($_[0]->{num})) } },
    'ceil'  => { args => 1, body => sub { Chessa::Number->new(num => POSIX::ceil($_[0]->{num})) } },
    'fact'  => { args => 1, body => sub { Chessa::Number->new(num => $fact->($_[0]->{num})) } },
    'max'   => { args => 0, body => sub { Chessa::Number->new(num => maxim(@_)) } },
    'min'   => { args => 0, body => sub { Chessa::Number->new(num => minim(@_)) } },
    'pcs'   => { args => 1, body => sub { Chessa::Number->new(num => stage(@_)) } },
    'rand'  => { args => 2, body => sub { Chessa::Number->new(num => int(rand($_[1]->{num})) + $_[0]->{num}) } },
);

sub parse_func($self) {
    my $name = $self->token_ident;

    my $arglist = $self->scope_of(
        '(', sub { $self->list_of(',', \&parse_expr) }, ')',
    );
    my @args = @$arglist;

    my ($lineno, $col, $text) = $self->where;

    my $argc = $functions{$name}{args};

    $argc = @args unless $argc > 0;

    croak "$lineno:$col:undefined function '$name'" unless exists($functions{$name});
    croak "$lineno:$col:'$name' wanted $argc arguments, got " . @args unless (@args >= $argc);

    return $functions{$name}{body}->(@args);
}

my %var = (
    pi   => sub { 4 * atan2(1, 1) },
    tau  => sub { 8 * atan2(1, 1) },
    e    => sub { exp(1) },
    pie	 => sub { (4 * atan2(1, 1)) * exp(1) },
);

sub _var($self, $var) {
    my ($lineno, $col, $text) = $self->where;

    croak "$lineno:$col:undefined variable '$var'" unless exists($var{$var});

    return $var{$var}->();
}

sub token_int($self, @args) {
    local $, = ", ";
    $self->generic_token(
        int => qr/-?(?:0b[01]+|0o[0-7]+|0x[0-9a-f]+|[0-9]+)/i,
        sub {
            my $str = $_[1];
            my $base = 10;
            my $sign = ( $str =~ s/^-// ) ? -1 : 1;

            my $val;
            if ($str =~ s/^0b// ) {
                $val = $sign * strtol($str, 2);
                $base = 2;
            }
            elsif ($str =~ s/^0o//) {
                $val = $sign * oct($str);
                $base = 8;
            }
            elsif ($str =~ s/^0x//) {
                $val = $sign * hex($str);
                $base = 16;
            }
            else {
                $val = $sign * $str;
            }
            return Chessa::Number->new(num => $val, base => $base);
        },
    );
}

sub parse_symbol($self) {
    $self->any_of(
        sub { Chessa::Number->new(num => $self->_var($self->token_ident)) },
        sub { Chessa::Number->new(num => $self->token_float) },
        sub { $self->token_int }
    );
};

1;

