package Chessa::Bug::Adelie;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use constant {
    name => 'Adelie',
};

use JSON::MaybeXS;
use DateTime::Format::ISO8601;
use Syntax::Keyword::Match;

sub new($class, $http, $log, $msg, $conf) {
    my $self = {};

    $self->{log} = $log;
    $self->{msg} = $msg;
    $self->{conf} = $conf;
    $self->{irc} = $self->{conf}{irc};
    $self->{http} = $http;
    $self->{projects} = {};

    load_projects($self, 'https://git.adelielinux.org/api/v4/projects');

    return bless $self, $class || ref $class;
}

sub log($self, $msg, $on_info=undef, $errors=0) {
    $self->{log}($msg);
    if (defined($on_info) && $errors) {
        $on_info->($msg);
    }
}
sub error($self, $msg, $on_info=undef, $errors=0) {
    $msg = $self->{msg}('Error', $msg);
    $self->log($msg, $on_info, $errors);
}

sub load_projects($self, $uri) {
    $self->{http}->do_request(
        uri         => $uri,
        on_response => sub ($response) {
            projects($self, $response);
        }, on_error => sub ($error) {
            $self->{log}("Couldn't fetch projects: $error");
            croak $error;
        }
    );
}

sub projects($self, $response) {
    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

    for my $item (@{ $json }) {
        $self->{projects}{$item->{path_with_namespace}} = $item->{id};
    }
    my $link = $response->header('Link');
    my @links = split /, /, $link;
    for my $i (@links) {
        if ($i =~ /"next"$/) {
            $i =~ /<(.*?)>/;
            $link = $1;
            load_projects($self, $link);
            last;
        }
    }
}

sub wants($self, $project) {
    if ($project =~ m!^adelie[/:]!i) {
        return 1;
    }
    return 0;
}

sub handle($self, $on_info, $data, $errors) {
    my ($project, $num, $type) = @{$data};

    $self->log("Got $project $type $num");

    my $base = 'https://git.adelielinux.org/api/v4/';
    my $func;
    if (!exists($self->{projects}{$project}) && ($type ne 'snippets')) {
        return;
    }
    if (length($project)) {
        $base .= 'projects/' . $self->{projects}{$project} . '/';
    }

    match($type : eq) {
        case ('issues') {
            $func = \&issue;
        } case ('merge_requests') {
            $func = \&mr;
        } case ('commits') {
            $base .= 'repository/';
            $func = \&commit;
        } case ('snippets') {
            $func = \&snippet;
        } default {
            return;
        }
    }
    $base .= "$type/$num";
    $self->log("Requesting $base");
    
    $self->{http}->do_request(
        uri         => $base,
        on_response => sub ($response) {
            $func->($self, $response, $on_info, $data, $errors);
        }, on_error => sub ($msg) {
            $self->log($self->{msg}('Error', $msg), $on_info, $errors);
        }
    );
}

sub issue($self, $response, $on_info, $data, $errors) {
    my ($project, $num, $type) = @{$data};
    
    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

    if (!$response->is_success) {
        $self->log($self->{msg}('Error', $json->{message}), $on_info, $errors);
        return;
    }

    my %d = (
        author => $json->{author}{name} . ' (@' . $json->{author}{username} . ')',
        title  => $json->{title},
        id     => $json->{iid},
        url    => $json->{web_url},
        state  => $json->{state},
    );
    my $str = $self->{msg}("$project#$d{id}", $d{title}) . ' (' .
        $self->{msg}('Status', $d{state}) . ' ' .
        $self->{msg}('Author', $d{author}) . ") - $d{url}";

    $on_info->($str);
}

sub mr($self, $response, $on_info, $data, $errors) {
    my ($project, $num, $type) = @{$data};

    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

    if (!$response->is_success) {
        $self->{log}($self->{msg}('Error', $json->{message}), $on_info, $errors);
        return;
    }

    my %d = (
        author => $json->{author}{name} . ' (@' . $json->{author}{username} . ')',
        title  => $json->{title},
        id     => $json->{iid},
        url    => $json->{web_url},
        state  => $json->{state},
        target => $json->{target_branch},
        source => $json->{source_branch},
        labels  => [],
        merge  => $json->{merge_status},
    );
    my @labels = ();
    $d{merge} =~ s/_/ /g;
    if (exists($json->{labels})) {
        for my $label (@{$json->{labels}}) {
            push @labels, $label;
        }
    }
    $d{labels} = \@labels;
    my $str = $self->{msg}("$project!$d{id}", $d{title}) . ' (' .
        $self->{msg}('Status', $d{state}) . "; $d{merge} " .
        $self->{msg}('Author', $d{author}) . ') ' .
        $d{source} . ' -> ' . $d{target};
    
    if (@{$d{labels}} > 0) {
        $str .= ' ' . $self->{msg}('Labels', join(' ', @{$d{labels}}));
    }

    $str .= " - $d{url}";

    $on_info->($str);
}

sub commit($self, $response, $on_info, $data, $errors) {
    my ($project, $num, $type) = @{$data};

    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

    if (!$response->is_success) {
        $self->{log}($self->{msg}('Error', $json->{message}), $on_info, $errors);
        return;
    }

    my %d = (
        author => $json->{author_name} . ' <' . $json->{author_email} . '>',
        commit => $json->{committer_name} . ' <' . $json->{committer_email} . '>',
        title  => $json->{title},
        id     => $json->{short_id},
        url    => $json->{web_url},
    );

    my $str = $self->{msg}("$project\@$d{id}", $d{title}) . ' (' .
        $self->{msg}('Author', $d{author}) . 
        ($d{author} ne $d{commit} ? ' ' . $self->{msg}('Committer', $d{commit}) : '') .
        ") - $d{url}";

    $on_info->($str);
}

sub snippet($self, $response, $on_info, $data, $errors) {
    my ($project, $num, $type) = @{$data};

    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);

    if (!$response->is_success) {
        $self->log($self->{msg}('Error', $json->{message}), $on_info, $errors);
        return;
    }

    my %d = (
        author => $json->{author}{name} . ' (@' . $json->{author}{username} . ')',
        title  => $json->{title},
        id     => $num,
        url    => $json->{web_url},
    );
    my $str = $self->{msg}("$project\$$d{id}", $d{title}) . ' (' .
        $self->{msg}('Author', $d{author}) . ") - $d{url}";
    
    $on_info->($str);
}

# A well-fed woof is a happy woof.
0xfeedbeef;
