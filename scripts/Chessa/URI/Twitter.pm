package Chessa::URI::Twitter;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use utf8;

use Carp qw(croak);
use HTML::Entities qw(decode_entities);
use List::Util qw(uniqstr);

use constant {
    name => 'Twitter',
};

use JSON::MaybeXS;
use DateTime::Format::ISO8601;

sub new($class, $http, $log, $msg, $conf) {
    my $self = {};

    $self->{log} = $log;
    $self->{msg} = $msg;
    $self->{conf} = $conf;
    $self->{http} = $http;

    if (!exists($conf->{bearer_token})) {
    	croak "bearer token required";
    }

    $self->{irc} = $self->{conf}{irc};

    return bless $self, $class || ref $class;
}


sub wants($self, $uri) {
    if (($uri->authority =~ /twitter\.com$/) && ($uri->path =~ m!status(?:es)?/\d+!)) {
        return 1;
    }
    return 0;
}

sub handle($self, $on_info, $uri, $errors) {
    $uri->as_string =~ m!status(?:es)?/(\d+)/?!;
    my $id = $1;

    $self->{http}->do_request(
        uri 		=> "https://api.twitter.com/2/tweets/$id?expansions=author_id,attachments.poll_ids,attachments.media_keys&poll.fields=voting_status&tweet.fields=text,created_at,referenced_tweets,attachments,entities,possibly_sensitive,reply_settings&user.fields=name,username,verified",
        headers		=> {'Authorization' => 'Bearer ' . $self->{conf}{bearer_token}},
        on_response => sub {
            my ($response) = @_;
            $self->get($response, $on_info, $id, $errors);
        }, on_error => sub {
            my ($msg) = @_;
            $self->{log}($self->{msg}('Error', "Couldn't get tweet with ID $id: $msg"), $on_info, $errors);
        }
    );
}

sub get($self, $response, $on_info, $id, $errors) {
    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);
    unless ($response->is_success) {
        if (defined($json->{errors})) {
            my @errors = ();

            for my $error (@{ $json->{errors} }) {
                push @errors, $error->{message};
            }

            my $str = $self->{msg}('Error', join(', ', @errors));
            
            $self->{log}("Unable to fetch tweet '$id': $str", $on_info, $errors);
        } else {
            $self->{log}("Unable to fetch tweet '$id': " . $response->status_line, $on_info, $errors);
        }
        return;
    }

    $on_info->($self->process_tweet($json));
}

our %tags = (
    quoted => 'quote',
    replied_to => 'reply',
);

sub process_tweet($self, $tweet) {
    # handle (displayname verified?): text Posted: datetime Mode: mode
    my $fmt = '%s (%s): %s %s: %s';

    my $text = $tweet->{data}{text};
    my $aid = $tweet->{data}{author_id};
    my ($author, @flags, $screen, $vbool);

    for my $user (@{$tweet->{includes}{users}}) {
    	if ($user->{id} eq $aid) {
    		$author = $user->{name};
    		$screen = $user->{username};
    		$vbool = !!$user->{verified};
    		last;
    	}
    }
    my $mode = $tweet->{data}{reply_settings};
    my $posted = DateTime::Format::ISO8601->parse_datetime($tweet->{data}{created_at});
    $posted = $posted->strftime("%Y-%m-%d %H:%M:%S UTC");

    if (exists($tweet->{data}{entities}{urls})) {
        for my $url (@{$tweet->{data}{entities}{urls}}) {
            my $base = quotemeta $url->{url};
            my $true = $url->{expanded_url};
            if ($true =~ m!/\w+/\d*$!) {
            	$text =~ s/ $base//;
            } else {
                $text =~ s/$base/$true/;
            }
        }
    }
    $text =~ s! ?https?://t\.co/[^ ]+!!g;
    $text =~ s/\r?\n/  /g;

    my $bold  = $self->{irc} ? "\cB" : '';
    my $end   = $self->{irc} ? "\cO" : '';
    my $green = $self->{irc} ? "\cC03" : '';
    my $red   = $self->{irc} ? "\cC05" : '';
    my $verify = $vbool ? "✔" : "✗";

    # flags
    if ($tweet->{data}{possibly_sensitive}) {
    	push @flags, 'nsfw';
    }
    if (exists($tweet->{includes}{media})) {
    	for my $media (@{$tweet->{includes}{media}}) {
    		push @flags, $media->{type};
    	}
    }
    if (exists($tweet->{includes}{polls})) {
    	for my $poll (@{$tweet->{includes}{polls}}) {
    		push @flags, $poll->{voting_status} . ' poll';
    	}
    }
    if (exists($tweet->{data}{referenced_tweets})) {
    	for my $ref (@{$tweet->{data}{referenced_tweets}}) {
    		push @flags, (exists($tags{$ref->{type}}) ? $tags{$ref->{type}} : $ref->{type});
    	}
    }
    if ($mode eq 'mentionedUsers') {
        push @flags, 'reply🔒mentions';
    } elsif ($mode eq 'following') {
        push @flags, 'reply🔒followers';
    }
    @flags = uniqstr(@flags);

    if (@flags > 0) {
    	$text .= " $bold\[$end" . join(',', @flags) . "$bold\]$end";
    }

    return decode_entities sprintf($fmt,
        "$bold$author$end",
        $screen . ($vbool ? " $green$verify$end" : " $red$verify$end"),
        $text,
        "${bold}Posted$end", $posted,
    );
}

# A well-fed woof is a happy woof.
0xfeedbeef;
