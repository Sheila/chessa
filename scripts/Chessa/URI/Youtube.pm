package Chessa::URI::Youtube;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use constant {
    name => 'Youtube',
};

use JSON::MaybeXS;

sub new($class, $http, $log, $msg, $conf) {
    my $self = {};

    $self->{log} = $log;
    $self->{msg} = $msg;
    $self->{conf} = $conf;
    $self->{http} = $http;

    return bless $self, $class || ref $class;
}

sub wants($self, $uri) {
    return ($uri->authority =~ /youtu(?:\.be|be\.com)(?:\:\d+)?$/)
}

sub handle($self, $on_info, $uri, $errors) {
    my ($item, $kind, $fetch);

    if ($uri->authority =~ /\.be(?:\:\d+)?$/) {
        $item = $uri->path;
        $item =~ s!^/!!;
        $kind = 'video';
    } elsif ($uri->path =~ m!/shorts!) {
        $item = $uri->path;
        $item =~ s!^/shorts/!!;
        $kind = 'video';
    } else {
        my %query = $uri->query_form;
        if (exists($query{v})) {
            ($item, $kind) = ($query{v}, 'video');
        } elsif (exists($query{list})) {
            ($item, $kind) = ($query{list}, 'playlist');
        }
    }

    if ($kind eq 'video') {
        $fetch = 'https://www.googleapis.com/youtube/v3/videos?id=%s&key=%s&fields=items(id,snippet(title,liveBroadcastContent,channelTitle),contentDetails(duration))&part=snippet,contentDetails';
    } elsif ($kind eq 'playlist') {
        $fetch = 'https://www.googleapis.com/youtube/v3/playlists?id=%s&key=%s&part=snippet,contentDetails&fields=items(snippet(title,channelTitle),contentDetails(itemCount))';
    } else {
        return;
    }

    $self->{http}->do_request(
        uri         => sprintf($fetch, $item, $self->{conf}{key}),
        on_response => sub ($response) {
            $self->get($response, $on_info, $kind, $item, $errors);
        }, on_error => sub ($msg) {
            $self->{log}($self->{msg}('Error', "Couldn't fetch $kind $item: $msg"), $on_info, $errors);
        }
    );
}

sub get($self, $response, $on_info, $kind, $item, $errors) {
    unless ($response->is_success) {
        $self->{log}("Unable to fetch $kind '$item': " . $response->status_line, $on_info, $errors);
        return;
    }

    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);
    unless ($json->{items} > 0) {
        $on_info->($self->{msg}('Error', "Unfortunately, $kind '$item' does not exist or has no information associated with it."));
        return;
    }

    my $output = '';
    if ($kind eq 'video') {
        $output = $self->output_video($json);
    } elsif ($kind eq 'playlist') {
        $output = $self->output_playlist($json);
    }

    $on_info->($output);
}

sub output_video($self, $json) {
    my $title  = $json->{items}[0]{snippet}{title};
    my $length = $json->{items}[0]{contentDetails}{duration};
    my $live   = $json->{items}[0]{snippet}{liveBroadcastContent};
    my $author = $json->{items}[0]{snippet}{channelTitle};

    my ($h, $m, $s);
    if ($length =~ /(\d+)H/) { $h = $1; }
    if ($length =~ /(\d+)M/) { $m = $1; } else { $m = 0 }
    if ($length =~ /(\d+)S/) { $s = $1; } else { $s = 0 }

    if (defined($h)) {
        $length = sprintf('%d:%02d:%02d', $h, $m, $s);
    } else {
        $length = sprintf('%d:%02d', $m, $s);
    }

    return $self->{msg}('Title', $title) . ' ' .
        ((defined($live) && ($live ne 'none')) ? $self->{msg}('Stream', $live) : $self->{msg}('Length', $length)) . ' ' .
        $self->{msg}('Uploader', $author);
}

sub output_playlist($self, $json) {
    my $list_title = $json->{items}[0]{snippet}{title};
    my $chan_title = $json->{items}[0]{snippet}{channelTitle};
    my $length     = $json->{items}[0]{contentDetails}{itemCount};

    return $self->{msg}("$chan_title presents", $list_title) . ' ' . $self->{msg}('Videos', $length);
}

# A well-fed woof is a happy woof.
0xfeedbeef;
