package Chessa::URI::Default;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use URI;
use HTML::TreeBuilder::XPath;
use Date::Format qw(time2str);
use HTML::Entities qw(decode_entities);
use Encode qw(encode);
use XML::XPath;
use JSON::MaybeXS;

use constant {
    name        => 'Default',
};

sub new($class, $http, $log, $msg, $conf) {
    my $self = {};

    $self->{log} = $log;
    $self->{msg} = $msg;
    $self->{conf} = $conf;
    $self->{irc} = $conf->{irc};
    $self->{http} = $http;

    return bless $self, $class || ref $class;
}

sub wants($self, $uri) {
    if ($uri->as_string =~ m!^https?://!) {
        return 1;
    } else {
        return 0;
    }
}

sub handle($self, $on_info, $uri, $errors) {
    $self->{http}->do_request(
        uri         => $uri,
        method      => 'HEAD',
        on_response => sub ($response) {
            $self->head($response, $uri, $on_info, $errors);
        }, on_error => sub ($msg) {
            $self->{log}($self->{msg}('Error', "Failed to fetch headers for $uri: $msg"), $on_info, $errors);
        },
    );
}

sub head($self, $response, $uri, $on_info, $errors) {
    my $authority = $uri->authority;
    $authority =~ s/^www\.(.*?)(?:\:.*)/$1/;

    unless ($response->is_success) {
        $self->{log}("Unable to fetch URI '$uri' via HEAD: " . $response->status_line, $on_info, $errors);
        return;
    }

    my $type = $response->header('Content-Type');
    my $size = $response->header('Content-Length');
    my $redirect = $self->redirect($response, $uri);

    return unless (defined($type) && ($type !~ /^\s*$/));

    if ($type =~ m!^(text/html|application/xhtml\+xml)!) {
        $self->{http}->do_request(
            uri         => $uri,
            method      => 'GET',
            on_response => sub ($response) {
                $self->get($response, $uri, $on_info, $redirect, $errors);
            }, on_error => sub ($msg) {
                $self->{log}($self->{msg}('Error', "Failed to fetch contents for $uri: " . $response->status_line), $on_info, $errors);
            },
        );
    } elsif ($type =~ m!^application/atom\+xml(?:$|;)!) {
        $self->{http}->do_request(
            uri         => $uri,
            method      => 'GET',
            on_response => sub ($response) {
                $self->get_atom($response, $uri, $on_info, $redirect, $errors);
            }, on_error => sub ($msg) {
                $self->{log}($self->{msg}('Error', "Failed to fetch contents for $uri: $msg", $on_info, $errors));
            },
        );
    } else {
        return if (!defined($size) || ($size < (10 * (1024 **2 ))));
        my $msg = $self->{msg}('Type', $type) . ' ' .
            $self->{msg}('Size', $self->humanize($size));
        if (defined($redirect) && (length($redirect) < 200)) {
            $msg .= ' ' . $self->{msg}('Redirect', $redirect);
        }
        $on_info->($msg);
    }
}

my @units = split //, 'kMGTPEZY';

sub humanize($self, $size) {
    my $radix = int(log($size) / log(1024));
    $radix = 9 if $radix > 9;

    my $val = $size / (1024 ** $radix);

    return sprintf("%.02f%s", $val, $units[$radix - 1] . 'iB');
}

sub redirect($self, $response, $uri) {
    my $redirects = $response->redirects;

    if ($redirects > 0) {
        return $response->base;
    }
    return undef;
}

sub get($self, $response, $uri, $on_info, $redirect, $errors) {
    my $authority = base_domain($uri);

    unless ($response->is_success) {
        $self->{log}("Unable to fetch URI '$uri' via GET: $response->status_line", $on_info, $errors);
        return;
    }

    my $t = HTML::TreeBuilder::XPath->new();
    $t->parse_content($response->decoded_content);
    my $title = undef;

    # Try to use AMP to get information, since some sites are more informative
    # with AMP than they are with regular HTML. Because, y'know, why be useful
    # to everyone when Google's the only people who matter?
    if ($t->exists('//script[@type="application/ld+json"]')) {
        $title = $self->ld_json($t, $authority);
        goto TITLE unless defined($title);
    } else {
TITLE:
        # Twitter card or OpenGraph
        if ($t->exists('//meta[@property="twitter:card"]') ||
            $t->exists('//meta[@property="og:title"]')) {
            $title = $self->tcard($t, $authority);
        } else {
            # This is not an AMP page, and neither twitter card nor OpenGraph
            # are provided.
            my @title = $t->findvalues('//title/text()');
            $title = $self->{msg}($authority, $title[0]);
        }
    }
    my $externals = $self->count_externals($t, $uri);

    if ($externals ne '') {
        $title .= ' ' . $externals;
    }
    if (defined($redirect) && (length($redirect) < 200)){
        $title .= ' ' . $self->{msg}('Redirect', $redirect);
    }

    $on_info->(decode_entities $title);
}

sub base_domain($uri) {
    $uri = URI->new($uri);
    my @temp = split /\./, $uri->authority;

    return $uri->authority if (@temp == 2);
    
    my @uri = ();
    my $j = 0;

    if ((length($temp[-1]) <= 3) && (length($temp[-2]) <= 3)) {
        $j = 3;
    } else {
        $j = 2;
    }
    for my $i (1 .. $j) {
        unshift @uri, pop @temp;
    }
    if ($uri[0] eq 'www') {
        shift @uri;
    }
    return join '.', @uri;
}

sub count_externals($self, $t, $source) {
    my $authority = base_domain($source);
    my @extemp = $t->findvalues('//@src');
    my $extcount = 0;
    my $totcount = scalar @extemp;

    foreach my $src (@extemp) {
        if ($src =~ m!^(http|://)!) {
            my $sauth = base_domain($src);
            if ($authority ne $sauth) {
                $extcount++;
            }
        }
    }
    return $self->{msg}('Assets', "$extcount/$totcount") if $totcount;
    return '';
}

sub ld_json($self, $t, $authority) {
    for my $i ($t->findvalues('//script[@type="application/ld+json"]/text()')) {
        my $json = JSON::MaybeXS->new->decode($i);
        # FIXME: The Atlantic's devs can suck deez nuts.
        next if ref($json) eq 'ARRAY';

        if (($json->{'@type'} eq 'WebPage') && (exists($json->{name}))) {
            my $title = $json->{name};
            $title =~ s/^\s+//; $title =~ s/\s+$//;
            return $self->{msg}($authority, $title);
        } elsif (exists($json->{headline})) {
            next unless ($json->{'@type'} =~ /^(NewsArticle|SocialMediaPosting)$/);
            my $title = $json->{headline};
            $title =~ s/^\s+//; $title =~ s/\s+$//;
            my $published = $json->{datePublished};
            my $author;
            if (ref($json->{author}) eq 'HASH') {
                $author = $json->{author}{name};
            } else {
                my @authors = ();
                foreach my $author (@{ $json->{author}}) {
                    push @authors, $author->{name};
                }
                my $author = join ', ', @authors;
            }
            return $self->{msg}($authority, $title) . ' ' .
                $self->{msg}('Author', $author) . ' ' .
                $self->{msg}('Published', $published);
        }
    }
    return undef;
}

my %properties = (
    title     => ['twitter:title', 'og:title'],
    author    => ['author', 'article:author'],
    desc      => ['twitter:description', 'og:description'],
    published => ['article:published_time'],
    modified  => ['article:modified_time'],
);

sub tcard($self, $t, $authority) {
    my %data = ();

    for my $prop (keys %properties) {
        for my $field (@{$properties{$prop}}) {
            if ($t->exists('//meta[@property="' . $field . '"]')) {
                $data{$prop} = $t->findvalue(
                    '//meta[@property="' . $field . '"]/@content'
                );
                last;
            }
        }
    }
    if (exists($data{author})) {
        if ($data{author} =~ /,/) {
            my @authors = split /, ?/, $data{author};
            $data{author} = join ', ', @authors;
        }
    }

    my $out = $self->{msg}($authority, $data{title});
    if (exists($data{author})) {
        $out .= ' ' . $self->{msg}('Author', $data{author});
    }
    if (exists($data{published})) {
        $out .= ' ' . $self->{msg}('Published', $data{published});
        if (exists($data{modified}) && ($data{published} ne $data{modified})) {
            $out .= ' (M: ' . $data{modified} . ')'; 
        }
    }
    my $size = 420 - length($out);

    if (length($data{desc}) > $size) {
        my @text = split ' ', $data{desc};
        my $data = '';
        my $x = 0;
        while ($x < $size) {
            my $word = shift @text;
            my $size = length($word);
            if (($x + $size + 1) < $size) {
                $data .= $word . ' ';
                $x += $size + 1;
            } else {
                last;
            }
        }
        $data .= '[...]';
        $data{desc} = $data;
    }
    if (length($data{desc})) {
        $out .= ' ' . $self->{msg}('Desc', $data{desc});
    }

    return $out;
}

sub get_atom($self, $response, $uri, $on_info, $redirect, $errors) {
    my $authority = base_domain($uri);
    my $xp = XML::XPath->new(xml => $response->decoded_content);

    my ($bold, $end) = ('', '');
    if ($self->{irc}) {
        $bold = "\cB";
        $end  = "\cO";
    }

    my $nsfw = lc($xp->findvalue('/entry/category/@term')) eq 'nsfw';
    my $author = $xp->findvalue('//author/name/text()');
    my $post = decode_entities $xp->findvalue('/entry/content/text()');
    my $attach = scalar $xp->findnodes('//link[@rel="enclosure"]');
    my $date = $xp->findvalue('/entry/published/text()');

    if ($xp->exists('//author/poco:displayName')) {
        $author = $bold . $xp->findvalue('//author/poco:displayName/text()') . $end . ' (@' . 
            $xp->findvalue('//author/poco:preferredUsername/text()') . '@' . $authority . ')';
    } else {
        $author = "$author (via $authority)";
    }
    # this presumes Pleroma, so it'll break if someone tags NSFW differently
    if ($nsfw) {
        $post = $xp->findvalue('//entry/summary/text()');
    }
    if (length($post) > 300) {
        my @text = split ' ', $post;
        my $out = '';
        my $x = 0;
        while ($x < 300) {
            my $word = shift @text;
            my $size = length($word);
            if (($x + $size + 1) < 300) {
                $out .= $word . ' ';
                $x += $size + 1;
            }
        }
        $out .= '[...]';
        $post = $out;
    }

    my $output = $author . ': ' . $post . ($nsfw ? " ${bold}[${end}content advisory$bold]$end" : '') . ($attach ? " ${bold}[${end}attachments$bold]$end" : '');

    $on_info->($output);
}

# A well-fed woof is a happy woof.
0xfeedbeef;
