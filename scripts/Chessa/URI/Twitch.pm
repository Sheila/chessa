package Chessa::URI::Twitch;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use JSON::MaybeXS;

use constant {
    name => 'Twitch',
};

sub new($class, $http, $log, $msg, $conf) {
    my $self = {};

    $self->{log} = $log;
    $self->{msg} = $msg;
    $self->{conf} = $conf;
    $self->{http} = $http;

    return bless $self, $class || ref $class;
}

sub wants($self, $uri) {
    if (($uri->authority =~ /twitch\.tv(?::\d+)?$/) && ($uri->path =~ m!^/[a-z0-9]+!)) {
        return 1
    }
}

sub handle($self, $on_info, $uri, $errors) {
    my ($fetch, $type) = ('', '');

    if ($uri->path =~ m!^/videos/([0-9]+)!) {
        $fetch = "videos?id=$1";
        $type = 'video';
    } elsif ($uri->path =~ m!^/([^/]+)!) {
        $fetch = "streams?user_login=$1";
        $type = 'channel';
    }

    $uri->path =~ m!^/([a-z0-9]+)!;

    $self->{http}->do_request(
        uri         => "https://api.twitch.tv/helix/$fetch",
        headers     => {'Client-ID' => $self->{conf}{id}},
        on_response => sub ($response) {
            $self->{log}("Fetched $type info ($uri), parsing HTML...");
            $self->get($response, $on_info, $type, $uri, $errors);
        }, on_error => sub ($msg) {
            $self->{log}("Couldn't fetch channel info ($uri); $msg");
            $on_info->($self->{msg}('Error', "Failed to fetch channel info ($uri): $msg")) if $errors;
        }
    );
}

sub get($self, $response, $on_info, $type, $uri, $errors) {
    unless ($response->is_success) {
        $self->{log}("Unable to fetch $type: $response->status_line", $on_info, $errors);
        return;
    }

    my $json = JSON::MaybeXS->new->utf8->decode($response->content);

    # something went wrong but we don't know what.
    if (@{ $json->{data} } < 1) {
        $self->{log}("Unable to fetch $type: no clue, Twitch fucked up", $on_info, $errors);
        return;
    }
    if ($type eq 'channel') {
        $on_info->($self->channel($uri, $json));
    } elsif ($type eq 'video') {
        $on_info->($self->video($uri, $json));
    }
}

sub channel($self, $uri, $json) {
    $uri->path =~ m!^/([a-z0-9]+)!;
    my $channel = $1;
    my $title = $json->{data}[0]{title};

    return $self->{msg}('Channel', $channel) . ' ' . $self->{msg}('Desc', $title);
}

sub video($self, $uri, $json) {
    return $self->{msg}('Title', $json->{data}[0]{title}) . ' ' . $self->{msg}('Desc', $json->{data}[0]{description});
}

# A well-fed woof is a happy woof.
0xfeedbeef;
