package Chessa::URI::Ignore;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use constant {
    name => 'Ignore',
};

sub new($class, $http, $log, $msg, $conf) {
    my $self = {};

    $self->{log} = $log;
    $self->{msg} = $msg;
    $self->{conf} = $conf;
    $self->{irc} = $conf->{irc};

    return bless $self, $class || ref $class;
}

sub wants($self, $uri) {
    my $host = $uri->host;

    return 1 if (
        ($host =~ /\bbloomberg\.com$/) ||
        ($host =~ /\bgoo\.gl$/) ||
        ($host =~ /docs\.google\.com$/)
    );

    return 0;
}

sub handle($self, $on_info, $uri, $errors=0) {
    return;
}

# A well-fed woof is a happy woof.
0xfeedbeef;
