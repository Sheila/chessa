package Chessa::URI::NPR;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use URI;
use HTML::TreeBuilder::XPath;

use constant {
	name => 'NPR'
};

sub new($class, $http, $log, $msg, $conf) {
	my $self = {};

	$self->{log} = $log;
	$self->{msg} = $msg;
	$self->{irc} = $conf->{irc};
	$self->{http} = $http;

	return bless $self, $class || ref $class;
}

sub wants($self, $uri) {
	return ($uri->authority =~ /\bnpr\.org$/) && ($uri->authority !~ /^text\b/);
}

sub handle($self, $on_info, $uri, $errors) {
    my $path = $uri->path;
    $path =~ m!/(\d{5,})/!;
    $uri = "https://text.npr.org/s.php?sId=$1";

    $self->{http}->do_request(
    	uri 		=> $uri,
    	on_response => sub ($response) {
    		$self->get($response, $uri, $on_info, $errors);
    	}, on_error => sub ($msg) {
    		my $error = $self->{msg}('Error', "Unable to fetch $uri: $msg");
    		$self->{log}($error);
    		$on_info->($error) if $errors;
    	}
    );
}

sub get($self, $response, $uri, $on_info, $errors) {
    unless ($response->is_success) {
        $self->{log}("Unable to fetch $uri: $response->status_line", $on_info, $errors);
        return;
    }
	my $p = HTML::TreeBuilder::XPath->new();
	$p->parse_content($response->decoded_content);
	my $title = $p->findvalue('//title/text()');
	$title =~ s/^.*:\s//;

	$on_info->($self->{msg}('Title', $title) . ' ' . $self->{msg}('Text URL', $uri));
}

# A well-fed woof is a happy woof.
0xfeedbeef;
