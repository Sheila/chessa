package Chessa::URI::GDocs;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use constant {
	name => 'GDocs'
};

use JSON::MaybeXS;

sub new($class, $http, $log, $msg, $conf) {
	my $self = {};

	$self->{log} = $log;
	$self->{msg} = $msg;
	$self->{irc} = $conf->{irc};
    $self->{http} = $http;

	return bless $self, $class || ref $class;
}

sub wants($self, $uri) {
	return $uri->authority =~ /^docs\.google\.com(?::\d+)$/;
}

sub handle($self, $on_info, $uri, $errors=0) {
    my $path = $uri->path;
    my $fetch;

    $path =~ m!/([a-z]+)/d/([a-zA-Z0-9_-])/!;
    my ($type, $id) = ($1, $2);

    if ($type eq 'spreadsheets') {
        # https://docs.google.com/spreadsheets/d/12AWopjvTWpPy2bv_C2oXiWb9Db-4H3WIAnsgMqKkdQ0/edit?usp=sharing
        $fetch = 'https://sheets.googleapis.com/v4/spreadsheets/';
    } elsif ($type eq 'document') {
        # https://docs.google.com/document/d/1DPE5u48Mn-GsdE80Hv2fPdW-KowtmwZ4hbmGyJHxtGY/edit?usp=sharing
        $fetch = 'https://docs.googleapis.com/v1/documents/';
    } else {
        $self->{log}("Unsupported Google Documents type '$type' with ID '$id'", $on_info, $errors);
        return;
    }

    $fetch .= $id . '?key=' . $self->{conf}{key};

    $self->{http}->do_request(
        uri => $fetch,
        on_response => sub ($response) {
            if ($type eq 'document') {
                $self->document($response, $id, $on_info, $errors);
            } elsif ($type eq 'spreadsheets') {
                $self->spreadsheets($response, $id, $on_info, $errors);
            }
        }, on_error => sub ($msg) {
            $self->{log}($self->{msg}('Error', "Failed to retrieve $type $id: $msg"), $on_info, $errors);
        }
    );
}

sub document($self, $response, $id, $on_info, $errors=0) {
    unless ($response->is_success) {
        $self->{log}("Unable to fetch document '$id': $response->status_line", $on_info, $errors);
        return;
    }
    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);
    my $title = $json->{title};

    $on_info->($self->{msg}('docs.google.com', $title));
}
sub spreadsheets($self, $response, $id, $on_info, $errors=0) {
    unless ($response->is_success) {
        $self->{log}("Unable to fetch document '$id': $response->status_line", $on_info, $errors);
        return;
    }
    my $json = JSON::MaybeXS->new->utf8->decode($response->decoded_content);
    my $title = $json->{properties}{title};

    $on_info->($self->{msg}('docs.google.com', $title));
}

# A well-fed woof is a happy woof.
0xfeedbeef;
