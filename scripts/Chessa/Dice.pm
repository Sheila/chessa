package Chessa::Dice;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use POSIX qw(floor);
use List::Util qw(max sum all);
use Try::Tiny;

use Chessa::Sharp;

my $p = Chessa::Sharp->new();
our %commands = (
    help => {
        usage => '<command>',
        desc  => 'Provides documentation on the command requested. As a special "command", "list" lists all commands for which help is available.'
    }
);
our %aliases = ();

sub help($input) {
    my $ret = '';

    my $command = '';
    if (exists($aliases{lc($input)})) {
        $command = $aliases{lc($input)};
    } elsif (exists($commands{lc($input)})) {
        $command = lc($input);
    } elsif (!defined($input) || (length($input) eq 0) || (lc($input) eq 'list')) {
        return "\cBCommands\cB: " . join(' ', keys %commands, keys %aliases);
    } else {
        return "No help available for '$input'; perhaps you misspelled it?";
    }

    return "\cBUsage\cB: .$command " . $commands{$command}{usage} . " \cBDescription\cB: " . $commands{$command}{desc};
}

sub _wrap($input) {
    my $output;

    try {
        $output = $p->from_string($input);
    } catch {
        $output = "\cBError\cB: $_";
        $output =~ s/ at.*//;
    };

    return $output;
}

$commands{roll} = {
    usage => '<formula1>[ <formula2>[...]]',
    desc  => 'Calculate multiple things at once, with special handling for dice. See http://kumiho.aerdan.org/chessa for more information.'
};
sub roll($input) {
    my @formulas = split ' ', $input;
    my $ret = "$input:";

    for my $formula (@formulas) {
        my @rolls = ();
        my $original = $formula;

        while ($formula =~ /((?:(?!0x[a-fA-F]))\d+d\d+)/) {
            my $roll = $1;
            $roll =~ /^(\d+)d(\d+)$/;
            my ($count, $sides) = ($1, $2);
            if ($count > 100) {
                return "Error: too many dice";
            }

            my @subrolls = ();

            for (my $i = 0; $i < $count; $i++) {
                my $temp = _wrap("1d$sides");
                return $temp if ($temp =~ /error/i);
                push @subrolls, $temp;
                push @rolls, $temp;
            }
            my $sum = sum(@subrolls);

            $formula =~ s/$roll/$sum/;
        }

        my $calc = _wrap($formula);

        return $calc if ($calc =~ /Error/);

        $ret .= " $calc (" . join(' ', @rolls) . ')';
    }

    return $ret;
}

$commands{math} = {
    usage => '<formula>',
    desc  => 'Calculate the given formula. Whitespace is permitted for readability. See http://kumiho.aerdan.org/chessa for more information.'
};
sub math($input) {
    return "$input: " . _wrap($input);
}

$commands{silcore} = {
    usage => '<dice> [bonus]',
    desc  => 'Rolls several six-sided dice, returning the highest roll (increased by the number of sixes after the first).'
};
sub silcore($input) {
    my ($dice, $bonus) = split ' ', $input, 2;

    $bonus = 0 unless defined $bonus;

    my ($highest, $sixes, @rolls) = (0, 0, ());

    for my $i (1..$dice) {
        my $tmp = _wrap('1d6');

        $highest = $tmp if ($tmp > $highest);

        if ($tmp == 6) {
            $sixes++;
            $tmp = "\cC03$tmp\cC";
        }
        push @rolls, $tmp;
    }

    $highest += $bonus + ($sixes > 0 ? $sixes - 1 : 0);

    return "${dice}d6+$bonus: " . join(' ', @rolls) . " (Result: $highest)";
}

$commands{exalted} = {
    usage => '<dice>[d[successes]] [target]',
    desc  => 'Roll several ten-sided dice. Rolls over the target number (default 7) are counted once. Tens count twice unless a d suffix appears, in which case they count as the number given (default 1).'
};
sub exalted($input) {
    my ($dice, $target) = split ' ', $input, 2;
    my $damage = 2;

    $dice = 1 unless defined($dice);
    $target = 7 unless defined($target);

    if ($dice =~ s/d(\d+)?$//) {
        $damage = $1 // 1;
    }

    $dice = 100 if ($dice > 100);

    my $ret = "${dice}d10 > $target (10 -> $damage): ";

    my ($successes, $ones, @rolls) = (0, 0, ());

    for (1..$dice) {
        my $roll = _wrap('1d10');

        if ($roll == 10) {
            $successes += $damage;
            $roll = $damage > 1 ? "\cC10$roll\cC" : "\cC03$roll\cC";
        } elsif ($roll > $target) {
            $successes++;
            $roll = "\cC03$roll\cC";
        } elsif ($roll == 1) {
            $ones++;
        }
        push @rolls, $roll;
    }

    $ret .= join(' ', @rolls);
    if ($successes > 0) {
        $ret .= " (\cBSuccesses\cB: $successes)";
    } elsif ($ones > 0) {
        $ret .= " (\cC04BOTCH!\cC)";
    }

    return $ret;
}

$commands{hellcats} = {
    usage => '[dice] [mod] [diff]',
    desc  => 'Rolls some six-sided dice. The mod, if given, decreases dice by a factor of three. Success is whether the highest die, plus bonus, exceeds the difficulty (default 4).'
};
sub hellcats($input) {
    my ($dice, $mod, $diff) = split ' ', $input, 3;
    $dice -= 3 * $mod if defined($mod);
    $mod = 0 unless defined($mod);

    my ($max, $eq, @rolls) = (0, '', ());
    push @rolls, _wrap('1d6') for (1..$dice);

    $max = max(@rolls) + $mod;

    if    ($max > $diff) { $eq = '>' }
    elsif ($max < $diff) { $eq = '<' }
    else                 { $eq = '=' }

    return "${dice}d6+$mod: " . join(' ', @rolls) . " ($max $eq $diff)";
}

$commands{fudge} = {
    usage => '[skill] [dice]',
    desc  => 'Rolls some Fudge dice (default 4) and adds the relevant skill (default 0). Supports the standard ladder for skill value, in addition to numbers.'
};
my @faces = ('-', '-', ' ', ' ', '+', '+');
my %ladder = (
    legendary =>  8,  '8' => 'Legendary',
    epic      =>  7,  '7' => 'Epic',
    fantastic =>  6,  '6' => 'Fantastic',
    superb    =>  5,  '5' => 'Superb',
    great     =>  4,  '4' => 'Great',
    good      =>  3,  '3' => 'Good',
    fair      =>  2,  '2' => 'Fair',
    average   =>  1,  '1' => 'Average',
    mediocre  =>  0,  '0' => 'Mediocre',
    poor      => -1, '-1' => 'Poor',
    terrible  => -2, '-2' => 'Terrible',
);
sub fudge($input) {
    my ($skill, $dice) = split ' ', $input, 2;

    $dice = 4 unless defined($dice);
    $skill = 0 unless defined($skill);
    $skill = $ladder{lc($skill)} if (($skill =~ /^-?\d+$/) && (exists($ladder{lc($skill)})));

    my @rolls = ();
    my $result = 0;

    for (1..$dice) {
        my $roll = _wrap('1d6-1') for (1..$dice);
        my $face = $faces[$roll];

        if ($face eq '-') {
            $result--;
        } elsif ($face eq '+') {
            $result++;
        }

        push @rolls, "[$face]";
    }

    return "${dice}dF+$skill: " . join('', @rolls) . ($result >= -2 ? " -- $ladder{$result}!" : '');
}

$commands{lonewolf} = {
    usage => '<Player> <Enemy> [override]',
    desc  => 'Determines the Combat Ratio, picks a random number, and displays the result. It will use the override number rather than pick if one is given.'
};
my %lonewolf = (
    '-11' => [[ 0, 'K'], [ 0, 'K'], [ 0,  -8], [  0, -8], [ -1, -7], [ -2, -6], [ -3, -5], [ -4, -4], [ -5, -3], [ -6, 0]],
    '-10' => [[ 0, 'K'], [ 0,  -8], [ 0,  -7], [ -1, -7], [ -2, -6], [ -3, -6], [ -4, -5], [ -5, -4], [ -6, -3], [ -7, 0]],
     '-9' => [[ 0, 'K'], [ 0,  -8], [ 0,  -7], [ -1, -7], [ -2, -6], [ -3, -6], [ -4, -5], [ -5, -4], [ -6, -3], [ -7, 0]],
     '-8' => [[ 0,  -8], [ 0,  -7], [-1,  -6], [ -2, -6], [ -3, -5], [ -4, -5], [ -5, -4], [ -6, -3], [ -7, -2], [ -8, 0]],
     '-7' => [[ 0,  -8], [ 0,  -7], [-1,  -6], [ -2, -6], [ -3, -5], [ -4, -5], [ -5, -4], [ -6, -3], [ -7, -2], [ -8, 0]],
     '-6' => [[ 0,  -6], [-1,  -6], [-2,  -5], [ -3, -5], [ -4, -4], [ -5, -4], [ -6, -3], [ -7, -2], [ -8,  0], [ -9, 0]],
     '-5' => [[ 0,  -6], [-1,  -6], [-2,  -5], [ -3, -5], [ -4, -4], [ -5, -4], [ -6, -3], [ -7, -2], [ -8,  0], [ -9, 0]],
     '-4' => [[-1,  -6], [-2,  -5], [-3,  -5], [ -4, -4], [ -5, -4], [ -6, -3], [ -7, -2], [ -8, -1], [ -9,  0], [-10, 0]],
     '-3' => [[-1,  -6], [-2,  -5], [-3,  -5], [ -4, -4], [ -5, -4], [ -6, -3], [ -7, -2], [ -8, -1], [ -9,  0], [-10, 0]],
     '-2' => [[-2,  -5], [-3,  -5], [-4,  -4], [ -5, -4], [ -6, -3], [ -7, -2], [ -8, -2], [ -9, -1], [-10,  0], [-11, 0]],
     '-1' => [[-2,  -5], [-3,  -5], [-4,  -4], [ -5, -4], [ -6, -3], [ -7, -2], [ -8, -2], [ -9, -1], [-10,  0], [-11, 0]],
      '0' => [[-3,  -5], [-4,  -4], [-5,  -4], [ -6, -3], [ -7, -2], [ -8, -2], [ -9, -1], [-10,  0], [-11,  0], [-12, 0]],
      '1' => [[-4,  -5], [-5,  -4], [-6,  -3], [ -7, -3], [ -8, -2], [ -9, -2], [-10, -1], [-11,  0], [-12,  0], [-14, 0]],
      '2' => [[-4,  -5], [-5,  -4], [-6,  -3], [ -7, -3], [ -8, -2], [ -9, -2], [-10, -1], [-11,  0], [-12,  0], [-14, 0]],
      '3' => [[-5,  -4], [-6,  -3], [-7,  -3], [ -8, -2], [ -9, -2], [-10, -2], [-11, -1], [-12,  0], [-14,  0], [-16, 0]],
      '4' => [[-5,  -4], [-6,  -3], [-7,  -3], [ -8, -2], [ -9, -2], [-10, -2], [-11, -1], [-12,  0], [-14,  0], [-16, 0]],
      '5' => [[-6,  -4], [-7,  -3], [-8,  -3], [ -9, -2], [-10, -2], [-11, -1], [-12,  0], [-14,  0], [-16,  0], [-18, 0]],
      '6' => [[-6,  -4], [-7,  -3], [-8,  -3], [ -9, -2], [-10, -2], [-11, -1], [-12,  0], [-14,  0], [-16,  0], [-18, 0]],
      '7' => [[-7,  -4], [-8,  -3], [-9,  -2], [-10, -2], [-11, -2], [-12, -1], [-14,  0], [-16,  0], [-18,  0], ['K', 0]],
      '8' => [[-7,  -4], [-8,  -3], [-9,  -2], [-10, -2], [-11, -2], [-12, -1], [-14,  0], [-16,  0], [-18,  0], ['K', 0]],
      '9' => [[-8,  -3], [-9,  -3], [-10, -2], [-11, -2], [-12, -2], [-14, -1], [-16,  0], [-18,  0], ['K',  0], ['K', 0]],
     '10' => [[-8,  -3], [-9,  -3], [-10, -2], [-11, -2], [-12, -2], [-14, -1], [-16,  0], [-18,  0], ['K',  0], ['K', 0]],
     '11' => [[-9,  -3], [-10, -2], [-11, -2], [-12, -2], [-14, -1], [-16, -1], [-18,  0], ['K',  0], ['K',  0], ['K', 0]],
);
sub lonewolf($input) {
    my ($player, $enemy, $override) = split ' ', $input, 3;
    my $ratio = $player - $enemy;

    if    ($ratio < -11) { $ratio = -11 }
    elsif ($ratio >  11) { $ratio =  11 }
    if    ($override < 0) { $override = 0 }
    elsif ($override > 9) { $override = 9 }

    my $roll = $override // _wrap('1d10-1');
    my $result = $lonewolf{$ratio}{$roll};

    $player = $result->[1];
    $enemy  = $result->[0];

    return "\cBCombat Ratio\cB: $ratio \cBRNT\cB: $roll \cBPlayer\cB: $player \cBEnemy\cB: $enemy";
}

$commands{dryh} = {
    usage => '<#d> [#emp]',
    desc  => 'Rolls several six-sided dice, with 1-3 being successes for their given pool. Each pool is given with the first letter of its name. The pool with the most successes wins; ties mean the roller wins.'
};
my %dryhtext = (
    d => 'Discipline',
    e => 'Exhaustion',
    m => 'Madness',
    p => 'Pain',
);
# I *really* don't like how ugly this implementation looks, but if anyone has any better ideas, I'm all ears.
# Roll Xd6 for X = pool size. Pool with most rolls in 1-3 range wins. If there's a tie, the roller wins.
sub dryh($input) {
    my @count = split ' ', $input;
    my %pools = ();

    for my $count (@count) {
        $count =~ /^(\d+)([demp])$/;
        my ($dice, $pool) = ($1, $2);

        $pools{$pool} = 0 unless exists($pools{$pool});
        $pools{$pool} += $dice;
    }

    return "\cBError\cB: Must have at least one Discipline die." unless exists($pools{d});

    my %rolls = ();

    for my $pool (keys %pools) {
        $rolls{$pool} = [];

        push @{$rolls{$pool}}, _wrap('1d6') for (1..$pools{$pool});
    }
    my ($maxr, $maxp, $tie, %successes) = (0, '', 0, ());

    for my $pool (keys %pools) {
        $successes{$pool} = grep { $_ =~ /^[123]$/ } @{ $rolls{$pool} };
        if ($successes{$pool} > $maxr) {
            $maxr = $successes{$pool};
            $maxp = $pool;
            $tie = 0;
        } elsif ($successes{$pool} == $maxr) {
            $tie = 1;
        }
    }

    my ($discipline, $exhaustion, $madness, $pain);
    my $fmt = '%d successes (%s)';
    $discipline = sprintf($fmt, $successes{d}, join(' ', @{ $rolls{d} }));
    $exhaustion = exists($pools{e}) ? sprintf($fmt, $successes{e}, join(' ', @{ $rolls{e} })) : '';
    $madness    = exists($pools{m}) ? sprintf($fmt, $successes{m}, join(' ', @{ $rolls{m} })) : '';
    $pain       = exists($pools{p}) ? sprintf($fmt, $successes{p}, join(' ', @{ $rolls{p} })) : '';

    my $ret = "Rolled $pools{d} Discipline" . 
        (exists($pools{e}) ? " $pools{e} Exhaustion" : '') .
        (exists($pools{m}) ? " $pools{m} Madness" : '') .
        (exists($pools{p}) ? " $pools{p} Pain" : '') .
        ": $discipline Discipline" .
        (exists($pools{e}) ? ", $exhaustion Exhaustion" : '') .
        (exists($pools{m}) ? ", $madness Madness" : '') .
        (exists($pools{p}) ? ", $pain Pain" : '') .
        ". \cBDominant\cB: " . ($tie ? 'Protagonist' : $dryhtext{$maxp});

    return $ret;
}

$commands{attrib} = {
    usage => '#[kd][hl][#]',
    desc  => 'Rolls six sets of six-sided dice. First number indicates dice per set, "d" and "k" mean "drop" and "keep", respectively, and "l" and "h" mean "lowest" and "highest", again respectively.'
};
sub attrib($input) {
    $input =~ /^(\d+)([dk][lh])(\d+)$/;
    my ($dice, $keep, $count) = ($1, $2, $3);

    unless (defined($keep)) {
        if (!defined($dice) || ($dice <= 3)) {
            $dice = 3;
            $keep = 'dl';
            $count = 0;
        } elsif ($dice > 3) {
            $keep = 'dl';
            $count = $dice - 3;
        }
    }

    my @pools = ();

    for my $i (0..5) {
        my @rolls = ();
        push @rolls, _wrap('1d6') for (1..$dice);
        my @sorted = sort { $a <=> $b } @rolls;

        if (($keep eq 'kl') || ($keep eq 'dh')) {
            @sorted = reverse @sorted;
        }
        splice @sorted, 0, $count;
        my $sum = sum(@sorted);

        push @pools, "$sum (" . join(' ', @rolls) . ')';
    }

    return "${dice}d6, " . ($keep =~ /^d/ ? 'dropping' : 'keeping') . " $count " . ($keep =~ /l$/ ? 'lowest' : 'highest') . ': ' . join(', ', @pools);
}

$commands{dw} = {
    usage => '<modifier>',
    desc  => 'Rolls two six-sided dice and adds the given modifier. Returns success level.'
};
sub dw($input) {
    return "\cBError\cB: not a number" unless ($input =~ /^-?\d+/);

    my @rolls = (_wrap('1d6'), _wrap('1d6'), $input);
    my $sum = sum(@rolls);
    pop @rolls;

    return "2d6+$input: $sum (" . join(' ', @rolls) . ')';
}

$commands{swrpg} = {
    usage => '[#bsadpcf...]',
    desc  => 'Rolls a collection of dice based on the die types specified according to the Star Wars RPG (Edge of the Empire, Age of Rebellion, and Force & Destiny) rules. (You must separate each dice pool with a space, e.g. "2a 2p".)'
};
$aliases{eote} = $aliases{aor} = $aliases{fad} = 'swrpg';
my %swrpg = (
    b => [6, ' ', ' ', 'rr', 'r', 'sr', 's'],
    s => [6, ' ', ' ', 'f',  'f', 'e',  'e'],
    a => [8, ' ', 's',  's', 'ss', 'r', 'r', 'sr', 'rr'],
    d => [8, ' ', 'f', 'ff',  'e', 'e', 'e', 'ee', 'fe'],
    p => [12, ' ', 's', 's', 'ss', 'ss', 'r', 'sr', 'sr', 'sr', 'rr', 'rr',  't'],
    c => [12, ' ', 'f', 'f', 'ff', 'ff', 'e',  'e', 'fe', 'fe', 'ee', 'ee',  'd'],
    f => [12, 'D', 'D', 'D',  'D',  'D', 'D', 'DD',  'l',  'l', 'll', 'll', 'll'],
);
my %swrpgtext = (
    b => 'Boost',
    s => 'Setback',
    a => 'Ability',
    d => 'Difficulty',
    p => 'Proficiency',
    c => 'Challenge',
    f => 'Force'
);
my @swrpgorder = qw(b s a d p c f);
sub swrpg($input) {
    my @count = split ' ', $input;

    my %pools = ();
    my $diecount = 0;

    for my $count (@count) {
        return "\cBError\cB: Invalid dice pool" unless ($count =~ /^(\d+)([bsadpcf])$/);
        my ($dice, $pool) = ($1, $2);

        $pools{$pool} = 0 unless exists($pools{$pool});
        $pools{$pool} += $dice;
        $diecount += $dice;
    }
    return "\cBError\cB: Dice pool required" unless $diecount > 0;

    my %rolls = ();
    my $results = '';

    for my $pool (keys %pools) {
        my @rolls = ();
        my $sides = $swrpg{$pool}[0];

        for (1..$pools{$pool}) {
            my $roll = _wrap("1d$sides");
            my $result = $swrpg{$pool}[$roll];
            $results .= $result;
            push @rolls, "$result:$roll";
        }
        $rolls{$pool} = \@rolls;
    }
    my %result = (
        triumph   => ($results =~ tr/t//),
        despair   => ($results =~ tr/d//),
        advantage => ($results =~ tr/r//) - ($results =~ tr/e//),
        light     => ($results =~ tr/l//),
        dark      => ($results =~ tr/D//),
    );
    $result{success} = (
        (($results =~ tr/s//) + $result{triumph}) -
        (($results =~ tr/f//) + $result{despair})
    );
    my $ret = "Rolled ";

    for my $pool (@swrpgorder) {
        next unless exists($pools{$pool});

        $ret .= "$pools{$pool} $swrpgtext{$pool} (" . join(' ', @{ $rolls{$pool} }) . ') ';
    }

    $ret .= ($result{success} > 0 ? "\c_\cC03Success\cO" : "\c_\cC05Failure\cO") . ": $result{success} ";
    $ret .= ($result{advantage} >= 0 ? "\c_\cC02Advantage\cO" : "\c_\cC06Threat\cO") . ": $result{advantage} ";
    $ret .= "\c_Triumph\c_: " . ($result{triumph} > 0 ? "\cC03$result{triumph}\cO " : "$result{triumph} ");
    $ret .= "\c_Despair\c_: " . ($result{despair} > 0 ? "\cC05$result{despair}\cO" : "$result{despair} ");
    $ret .= "\c_Light\c_: $result{light} \c_Dark\c_: $result{dark}";

    return $ret;
}

my %pfbuy = (
     7 => -4,  8 => -2,  9 => -1,
    10 =>  0, 11 =>  1, 12 =>  2,
    13 =>  3, 14 =>  5, 15 =>  7,
    16 => 10, 17 => 13, 18 => 17,
);
$commands{pfbuy} = {
    usage => '<Abilities>',
    desc  => 'Evaluates the given ability array and returns the cost to use that array.'
};
sub pfbuy($input) {
    my @abilities = split ' ', $input, 6;
    my $cost = 0;

    for my $ability (@abilities) {
        if (exists($pfbuy{$ability})) {
            $cost += $pfbuy{$ability};
        } else {
            return "\cBError\cB: Invalid ability score: $ability";
        }
    }

    return 'This ability score array (' . join(', ', @abilities) . ") costs $cost points.";
}

$commands{explode} = {
    usage => '<dice> <explode> <success>',
    desc  => 'Rolls the given dice. Rolls equal to or above <success> (default: sides / 2 + 1) are green, rolls equal to or above <explode> (default: sides) are bold and rerolled, with the new number also added to the pool.'
};
sub explode($input) {
    my ($dice, $explode, $success) = split ' ', $input, 3;

    return "\cBError\cB: dice only: $dice" unless ($dice =~ /^(\d+)d(\d+)$/);
    my ($count, $sides) = ($1, $2);
    $explode = $sides unless defined($explode);
    $success = floor($sides / 2) + 1 unless defined($success);
    return "\cBError\cB: unachievable: $success success > $sides" unless ($success <= $sides);
    return "\cBError\cB: unachievable: $explode explode > $sides" unless ($explode <= $sides);

    my @rolls = ();
    my $exploders = 0;

    while ($count > 0) {
        my $roll = _wrap("1d$sides");
        my $temp = $roll;

        $count-- if ($roll < $explode);

        if ($temp >= $success) {
            $roll = "\cC03$roll\cC";
        }
        if ($temp >= $explode) {
            $roll = "\cB$roll\cB";
            $exploders++;
        }

        push @rolls, $roll;
    }

    return "$dice, success >= $success, exploding >= $explode: " . join(' ', @rolls) . " ($exploders explosions)";
}

$commands{cortex} = {
    usage => '<4,6,8,10,12> [...]',
    desc  => "Rolls the given dice and returns the results. This is a convenience function for Cortex Prime.",
};
sub cortex($input) {
    my @dice = split ' ', $input;
    my @rolls = ();
    my $ones = 0;

    for my $die (@dice) {
        return "\cBError\cB: Invalid die '$die'" unless ($die =~ /^(1[02]|[468])$/);
        my $temp = _wrap("1d$die");
        push @rolls, $temp;
        $ones++ if $temp == 1;
    }
    return join(' ', @dice) . ': ' . join(' ', @rolls) . ($ones == @rolls) ? " (\cC05botch!\cO)" : '';
}
$commands{imperialassault} = {
    usage => '[rugybw]+',
    desc  => 'Rolls the requested coloured dice for Imperial Assault. Note that "u" is the blue die and "b" is the black die.'
};
$aliases{ia} = 'imperialassault';
my %imperial = (
    r => [0, 'd', 'dd', 'dd', 'dds', 'ddd', 'ddd'],
    u => [0, 'saa', 'daa', 'ddaaa', 'dsaaa', 'ddaaaa', 'daaaaa'],
    g => [0, 'sa', 'dsa', 'dda', 'dsaa', 'ddaa', 'ddaaa'],
    y => [0, 's', 'dss', 'dda', 'dsa', 'saa', 'daa'],
    b => [0, 'b', 'b', 'bb', 'bb', 'bbb', 'e'],
    w => [0, ' ', 'b', 'e', 'be', 'be', 'D']
);
sub imperialassault($input) {
    my @count = split //, lc $input;
    my @res = ();
    my @rolls = ();
    $input =~ s/\s*//g;

    return "\cBError\cB: Can't mix action and defense pools." if ($input =~ /[rugy]/) && ($input =~ /[bw]/);
    return "\cBError\cB: Unknown colours" if ($input =~ /[^rugybw]/);

    for my $die (@count) {
        my $roll = _wrap('1d6');
        push @res, $imperial{$die}[$roll];
        push @rolls, $roll;
    }
    my $results = join '', @res;
    @res = split //, $results;
    my %pools = ();

    for my $token (@res) {
        $pools{$token}++
    }
    my $label = ($input =~ /[bw]/) ? 'Defense' : 'Action';

    my $output = 'Rolled ' . scalar(@rolls) . "d6 for $label: " . join ' ', @rolls;
    if ($label eq 'Action') {
        $pools{d} = 0 unless exists($pools{d});
        $pools{s} = 0 unless exists($pools{s});
        $pools{a} = 0 unless exists($pools{a});
        $output .= " ($pools{d} damage, $pools{a} accuracy, $pools{s} surge)";
    } elsif ($label eq 'Defense') {
        $pools{b} = 0 unless exists($pools{b});
        $pools{e} = 0 unless exists($pools{e});
        $pools{D} = 0 unless exists($pools{D});
        $output .= " ($pools{b} block, $pools{e} evade, $pools{D} dodge)";
    }

    return $output;
}

$commands{blade} = {
    'usage' => '<#>',
    'desc'  => 'Rolls the requested number of d6\'s. Outcome is based on the highest die; 6 is a success (more than one is a critical success), 4-5 is a partial success, and 1-3 is a bad outcome. Zero or negative dice rolls two dice and uses the lower for the outcome.'
};
$aliases{bitd} = 'blade';
sub blade($input) {
    my ($outcome, $dice) = (1, $input);
    if ($input < 1) {
        $outcome = 0;
        $dice = 2;
    } else {
        $dice = int($input);
    }
    my $max = 0;
    my $min = 7;
    my @rolls = ();

    for (my $i = 0; $i < $dice; $i++) {
        my $temp = _wrap('1d6');
        if ($max < $temp) {
            $max = $temp;
        } elsif (($max == 6) && ($temp == 6)) {
            $outcome++ if $outcome == 1;
        }
        if ($min > $temp) {
            $min = $temp;
        }
        push @rolls, $temp;
    }
    my $output = "${dice}d6: " . join(' ', @rolls);
    if ($outcome == 2) {
        $output .= " (Critical success!)";
    } elsif ($outcome == 1) {
        if ($max == 6) {
            $output .= " (Success!)";
        } elsif ($max > 3) {
            $output .= " (Partial success.)";
        } else {
            $output .= " (Bad outcome...)";
        }
    } else {
        if ($min == 6) {
            $output .= " (Critical success!)";
        } elsif ($min > 3) {
            $output .= " (Partial success.)";
        } else {
            $output .= " (Bad outcome...)"
        }
    }

    return $output;
}

# A well-fed woof is a happy woof.
0xfeedbeef;
