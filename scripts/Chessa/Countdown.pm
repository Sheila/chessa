package Chessa::Countdown;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use DateTime;
use DateTime::Format::Duration;
use DateTime::Format::Strptime;
use Data::Dumper qw(Dumper);

sub new($class, %args) {
    my $date = delete $args{date} // croak 'date required';
    my $strp = DateTime::Format::Strptime->new(
        pattern => '%Y-%m-%dT%T',
        time_zone => 'US/Central',
        on_error => 'croak',
    );

    if ($date =~ /^%Y/) {
        my $date2 = $date;
        my $d = DateTime->now();
        my $y = $d->year;

        $date2 =~ s/%Y/$y/;
        my $d2 = $strp->parse_datetime($date2);

        if ($d > $d2) {
            $y++;
            $date =~ s/%Y/$y/;
        } else {
            $date = $date2;
        }
    }

    my $self = {
        date => $strp->parse_datetime($date),
        strp => $strp,
    };

    return bless $self, $class || ref $class;
}

sub delta($self) {
    my $self = shift;
    my $now = DateTime->now(time_zone => 'US/Central');
    my $date = $self->{date}->clone;

    my $delta = $date - $now;

    my $temp = DateTime::Format::Duration->new(
        pattern => '%j days, %H:%M:%S',
        normalise => 1,
    );

    my $ret = $temp->format_duration($delta);

    $ret =~ s/days/day/ if ($ret =~ /^1 /);

    return $ret;
}

1;

