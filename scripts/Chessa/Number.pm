package Chessa::Number;
use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

sub new($class, %args) {
    my $self = {
        num => undef,
        base => 10
    };

    $self->{base} = delete $args{base} if defined($args{base});
    $self->{num} = delete $args{num} if defined($args{num});

    return bless $self, $class || ref $class;
}

sub to_string($self, $num=0, $base=undef) {
    if (defined($self->{num})) {
        $num = $self->{num};
    } elsif (!defined($num)) {
        $num = 0;
    }
    if (defined($self->{base})) {
        $base = $self->{base};
    }
    if    ($base == 2)  { sprintf '0b%b', $num; }
    elsif ($base == 8)  { sprintf '0o%o', $num; }
    elsif ($base == 16) { sprintf '0x%X', $num; }
    else {
        if ($num =~ /(\.|^1e)/) {
            sprintf '%f', $num;
        } else {
            sprintf '%d', $num;
        }
    }
}

sub _retrieve($self, @args) {
    my ($lhs, $rhs, $base);

    if (!defined($self->{num})) {
        ($lhs, $rhs) = @args;
    } else {
        $lhs = $self;
        $rhs = $args[0];
    }

    if ((ref($lhs) eq 'Chessa::Number') && (ref($rhs) eq 'Chessa::Number')) {
        if ($lhs->{base} == $rhs->{base}) {
            $base = $lhs->{base};
        } elsif ($lhs->{base} == 10) {
            $base = $rhs->{base};
        } elsif ($rhs->{base} == 10) {
            $base = $lhs->{base};
        } else {
            $base = 10;
        }

        $lhs = $lhs->{num};
        $rhs = $rhs->{num};
    } elsif (ref($lhs) eq 'Chessa::Number') {
        $base = $lhs->{base};
        $lhs = $lhs->{num};
    } elsif (ref($rhs) eq 'Chessa::Number') {
        $base = $rhs->{base};
        $rhs = $rhs->{num};
    }

    return ($lhs, $rhs, $base);
}

sub exp($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    return Chessa::Number->new(num => $lhs ** $rhs, base => $base);
}

sub add($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    return Chessa::Number->new(num => $lhs + $rhs, base => $base);
}

sub sub_($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    return Chessa::Number->new(num => $lhs - $rhs, base => $base);
}

sub mul($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    return Chessa::Number->new(num => $lhs * $rhs, base => $base);
}

sub div($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    return Chessa::Number->new(num => $lhs / $rhs, base => $base);
}

sub mod($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    return Chessa::Number->new(num => $lhs % $rhs, base => $base);
}

sub band($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    $lhs = int($lhs + 0.5) if (int($lhs) < $lhs);
    $rhs = int($rhs + 0.5) if (int($rhs) < $rhs);
    return Chessa::Number->new(num => $lhs & $rhs, base => $base);
}
sub bor($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    $lhs = int($lhs + 0.5) if (int($lhs) < $lhs);
    $rhs = int($rhs + 0.5) if (int($rhs) < $rhs);
    return Chessa::Number->new(num => $lhs | $rhs, base => $base);
}

sub bxor($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    $lhs = int($lhs + 0.5) if (int($lhs) < $lhs);
    $rhs = int($rhs + 0.5) if (int($rhs) < $rhs);
    return Chessa::Number->new(num => $lhs ^ $rhs, base => $base);
}

sub lsh($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    $lhs = int($lhs + 0.5) if (int($lhs) < $lhs);
    $rhs = int($rhs + 0.5) if (int($rhs) < $rhs);
    return Chessa::Number->new(num => $lhs << $rhs, base => $base);
}

sub rsh($self, @args) {
    my ($lhs, $rhs, $base) = $self->_retrieve(@args);

    $lhs = int($lhs + 0.5) if (int($lhs) < $lhs);
    $rhs = int($rhs + 0.5) if (int($rhs) < $rhs);
    return Chessa::Number->new(num => $lhs >> $rhs, base => $base);
}

sub abs($self) {
    return Chessa::Number->new(num => abs $self->{num}, base => $self->{base});
}

sub neg($self) {
    return Chessa::Number->new(num => -$self->{num}, base => $self->{base});
}

sub bneg($self) {
    return Chessa::Number->new(num => ~$self->{num}, base => $self->{base});
}

1;
