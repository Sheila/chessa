package Chessa::Database::Pronouns;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use Syntax::Keyword::Try;

use constant {
    name => 'Pronouns',
};

sub new($class, $conf, $db, $msg, $error) {
    my $self = {};

    $self->{conf}  = $conf;
    $self->{db}    = $db;
    $self->{msg}   = $msg;
    $self->{error} = $error;

    return bless $self, $class || ref $class;
}

sub init_database($self) {
    my $sth = $self->{db}->prepare('CREATE TABLE IF NOT EXISTS pronouns (id INTEGER PRIMARY KEY, name VARCHAR(32), network VARCHAR(64), pronouns VARCHAR(64));');
    $sth->execute();
    return "Error: " . $sth->errstr if $sth->err;
}

sub help($self) {
    return "Usage: .pronouns [find <user>|set <pronouns>|set <user> <pronouns>|<user>]  Desc: <pronouns> is freeform, but male/female/neutral are convenience values which work as one would expect.";
}

sub find($self, $name, $network) {
    my $sth = $self->{db}->prepare('SELECT pronouns FROM pronouns WHERE name=? AND network=?;');
    $sth->execute($name, $network);

    if ($sth->err) {
        return "Error: No such user.";
    } else {
        my $temp = $sth->fetchrow_arrayref;
        return "Error: No such user." unless defined($temp->[0]);
        return "Pronouns for $name are: " . $temp->[0];
    }
}

my %predefined = (
    male    => 'he/him/his',
    female  => 'she/her/hers',
    neutral => 'they/them/their',
);

sub set($self, $name, $network, $pronouns) {
    if ($pronouns =~ /(attack helicopter|mayonn?aiss?e|:)/i) {
        return "Error: Invalid pronouns";
    }

    $pronouns = $predefined{lc $pronouns} if exists($predefined{lc $pronouns});

    my $sth = undef;
    if ($self->find($name, $network) =~ /^Error: /) {
        $sth = $self->{db}->prepare('INSERT INTO pronouns (pronouns, name, network) VALUES (?, ?, ?);');
    } else {
        $sth = $self->{db}->prepare('UPDATE pronouns SET pronouns=? WHERE name=? AND network=?;');
    }

    $sth->execute($pronouns, $name, $network);

    if ($sth->err) {
        return "Error: " . $sth->errstr;
    }

    return "Set pronouns for $name to '$pronouns'.";
}

sub del($self, $name, $network) {

    if ($self->find($name, $network) =~ /^Error: /) {
        return "Error: No such user."
    }
    my $sth = $self->{db}->prepare('DELETE FROM pronouns WHERE name=? AND network=?;');

    $sth->execute($name, $network);

    if ($sth->err) {
        return "Error: " . $sth->errstr;
    }

    return "Deleted $name from database.";
}

# A well-fed woof is a happy woof.
0xfeedbeef;
