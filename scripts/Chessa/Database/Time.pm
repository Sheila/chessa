package Chessa::Database::Time;

use strict;
use warnings;
use feature qw(:5.30 signatures);
no warnings qw(experimental::signatures);

use Carp qw(croak);

use DateTime;
use DateTime::TimeZone;

use constant {
    name => 'Time',
};

sub new($class, $conf, $db, $msg, $error) {
    my $self = {};

    $self->{conf}  = $conf;
    $self->{db}    = $db;
    $self->{msg}   = $msg;
    $self->{error} = $error;

    return bless $self, $class || ref $class;
}

sub init_database($self) {
    my $sth = $self->{db}->prepare('CREATE TABLE IF NOT EXISTS time (id INTEGER PRIMARY KEY, name VARCHAR(32), network VARCHAR(64), timezone VARCHAR(64));');
    $sth->execute();
    return "Error: " . $sth->errstr if $sth->err;
}

sub help($self) {
    return "Usage: .time [find <timezone>|set <timezone>|<user>]  Desc: Find out what time it is in a given timezone or for a given user. See DateTime::TimeZone's timezone listing if something that seems valid is rejected.";
}

sub tz($self, $tz) {
    return "Error: Unrecognized timezone: $tz" unless DateTime::TimeZone->is_valid_name($tz);
    return "Time in $tz is: " . DateTime->now(time_zone => $tz);
}

sub find($self, $name, $network) {
    my $sth = $self->{db}->prepare('SELECT timezone FROM time WHERE name=? AND network=?;');
    $sth->execute($name, $network);
    my $istz = DateTime::TimeZone->is_valid_name($name);

    if ($sth->err) {
        my $out = $self->tz($name);
        return $out unless $out =~ /^Error/;
        return "Error: No such user.";
    } else {
        my @temp = $sth->fetchrow_array;
        if ($istz || (@temp == 0)) {
            my $out = $self->tz($name);
            return $out unless $out =~ /^Error/;
            return "Error: No such user.";
        }
 
        my $tz = quotemeta($temp[0]);
        my $msg = $self->tz($temp[0]);
        $msg =~ s/in $tz/for $name/;

        return $msg;
    }
}

sub set($self, $name, $network, $timezone) {
    if (DateTime::TimeZone->is_valid_name($name)) {
        return;
    }

    my $sth = $self->{db}->prepare(
        'INSERT INTO time (timezone, name, network) VALUES (?1, ?2, ?3) ON CONFLICT (name, network) DO UPDATE SET timezone=?1;'
    );

    $sth->execute($timezone, $name, $network);

    if ($sth->err) {
        return "Error: " . $sth->errstr;
    }

    return "Set timezone for $name to '$timezone'.";
}

sub del($self, $name, $network) {
    if ($self->find($name, $network) =~ /^Error: /) {
        return "Error: No such user."
    }
    my $sth = $self->{db}->prepare('DELETE FROM time WHERE name=? AND network=?;');

    $sth->execute($name, $network);

    if ($sth->err) {
        return "Error: " . $sth->errstr;
    }

    return "Deleted $name from database.";
}

# A well-fed woof is a happy woof.
0xfeedbeef;
